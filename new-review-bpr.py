from recommender.ReviewBPR_NEW import ReviewBPR_NEW
from recommender.DeepCoNN import DeepCoNN
from recommender.BPR import BPRRecommender
from recommender.SVDPP import SVDPP
from data_model.ReviewDataModel import ReviewDataModel

def runParm(config, fileName, lam):
    config['fileName'] = fileName
    dataModel = ReviewDataModel(config)
    dataModel.buildModel()
    config['factor_lambda'] = lam
    config['atten_lambda'] = lam
    config['review_lambda'] = lam
    config['dropout_keep_prob'] = 0.8
    rec = ReviewBPR_NEW(dataModel, config)
    rec.run()

if __name__ == '__main__':

    # digital_music_5_core_100, baby_5_core_100 apps_for_android_5
    # kindle_5 movies_tv_5
    config = {
        'negative_sample': 0,
        'splitterType': 'userTimeRatio',
        'fileName': 'electronics_5',
        'trainType': 'test',
        'threshold': 0,
        'wordVecSize': 200,
        'learnRate': 0.001,
        'maxIter': 500,
        'trainBatchSize': 10,
        'testBatchSize': 10,
        'numFactor': 32,
        'topN': 10,
        'factor_lambda': 0.1,
        'atten_lambda': 0.1,
        'goal': 'ranking',
        'addBias': False,
        'with_review_input': True,
        'attention_version': 'new',
        'dropout_keep_prob': 0.8,
        'verbose': False,
        'item_pad_num': 14,
        'if_full_review': True,
        'review_lambda': 0.1,

        'component_type': 'RNN',

        'rnn_unit_num': 48,
        'rnn_layer_num': 1,
        'rnn_cell': 'GRU',

        'filters_sizes': [2, 3],
        'num_filters': 50,
        'atten_level': 'no_current',
        'add_relu': False,
        'feature_atten': False,

        'random_seed': 1,
        'eval_item_num': 100,
        'early_stop': True,

        'save_model': False,
        'load_model': False,
    }


    for fileName in ["kindle_5", "electronics_5", "apps_for_android_5", "cd_vinyl_5", "movies_tv_5"]:
        config['fileName'] = fileName
        dataModel = ReviewDataModel(config)
        dataModel.buildModel()

        for seed in range(10):
            config['random_seed'] = seed
            rec = ReviewBPR_NEW(dataModel, config)
            rec.run()

    # for atten_level in ['item-item', 'item-item-review']:
    #     config['component_type'] = 'RNN'
    #     config['atten_level'] = atten_level
    #     rec = ReviewBPR_NEW(dataModel, config)
    #     rec.run()


    # # testing dimension size
    # config['component_type'] = 'RNN'
    # for num_facotr in [8, 16, 32, 64]:
    #     config['numFactor'] = num_facotr
    #     config['factor_lambda'] = 0.01
    #     config['atten_lambda'] = 0.01
    #     config['review_lambda'] = 0.01
    #     rec = ReviewBPR_NEW(dataModel, config)
    #     rec.run()
    #
    # config['component_type'] = 'CNN'
    # for num_facotr in [8, 16, 32, 64]:
    #     config['factor_lambda'] = 1.0
    #     config['atten_lambda'] = 1.0
    #     config['review_lambda'] = 1.0
    #     config['numFactor'] = num_facotr
    #     rec = ReviewBPR_NEW(dataModel, config)
    #     rec.run()
    #
    # config['fm_k'] = 8
    # for num_facotr in [8, 16, 32, 64]:
    #     config['numFactor'] = num_facotr
    #     rec = DeepCoNN(dataModel, config)
    #     rec.run()
    #
    # for num_facotr in [8, 16, 32, 64]:
    #     config['factor_lambda'] = 0.1
    #     config['atten_lambda'] = 0.1
    #     config['review_lambda'] = 0.1
    #     config['learnRate'] = 0.01
    #     config['numFactor'] = num_facotr
    #     rec = BPRRecommender(dataModel, config)
    #     rec.run()
    #     config['learnRate'] = 0.01
    #
    #
    # # testing dropout rate
    #
    # # run ADR-CNN
    # config['component_type'] = 'CNN'
    #
    # for drop_keep in [1.0, 0.8, 0.6, 0.4, 0.2]:
    #     config['factor_lambda'] = 1.0
    #     config['atten_lambda'] = 1.0
    #     config['review_lambda'] = 1.0
    #     config['dropout_keep_prob'] = drop_keep
    #     rec = ReviewBPR_NEW(dataModel, config)
    #     rec.run()
    #
    # # run ADR-RNN
    # config['component_type'] = 'RNN'
    #
    # for drop_keep in [1.0, 0.8, 0.6, 0.4, 0.2]:
    #     config['factor_lambda'] = 0.001
    #     config['atten_lambda'] = 0.001
    #     config['review_lambda'] = 0.001
    #     config['dropout_keep_prob'] = drop_keep
    #     rec = ReviewBPR_NEW(dataModel, config)
    #     rec.run()

    # run DeepCoNN
    # config['fm_k'] = 8
    # for drop_keep in [0.4]:
    #     config['dropout_keep_prob'] = drop_keep
    #     rec = DeepCoNN(dataModel, config)
    #     rec.run()


    # testing lambda


    # runParm(config=config, fileName='electronics_5', lam=0.001)
    # runParm(config=config, fileName='apps_for_android_5', lam=1.0)

    # config['component_type']='CNN'
    # runParm(config=config, fileName='cd_vinyl_5', lam=1.0)
    # runParm(config=config, fileName='electronics_5', lam=1.0)
    # runParm(config=config, fileName='apps_for_android_5', lam=0.1)




    # for fileName in ['cd_vinyl_5', 'electronics_5', 'apps_for_android_5']:
    #     config['fileName'] = fileName
    #     dataModel = ReviewDataModel(config)
    #     dataModel.buildModel()
    #     for lam in [1.0]:
    #         for dropout in [0.8]:
    #             config['factor_lambda'] = lam
    #             config['atten_lambda'] = lam
    #             config['review_lambda'] = lam
    #             config['dropout_keep_prob'] = dropout
    #             rec = ReviewBPR_NEW(dataModel, config)
    #             rec.run()

    # for lam in [0.1, 1]:
    #     for dropout in [0.8]:
    #         config['factor_lambda'] = lam
    #         config['rnn_lambda'] = lam
    #         config['atten_lambda'] = lam
    #         config['dropout_keep_prob'] = dropout
    #         rec = recommender.ReviewBPR_RNN.ReviewBPR(dataModel, config)
    #         rec.run()



    # for learnrate in [0.001, 0.01, 0.1]:
    #     for batchSize in [2000, 1000, 500, 250, 128]:
    #         config['learnRate'] = learnrate
    #         config['trainBatchSize'] = batchSize
    #         rec = recommender.ReviewBPR_RNN.ReviewBPR(dataModel, config)
    #         rec.run()








# 1.0, 0.9, 0.8, 0.7, 0.6, 0.5







