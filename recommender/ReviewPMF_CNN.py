import numpy as np
import tensorflow as tf
import random
import recommender.BasicRcommender

class ReviewAttention(recommender.BasicRcommender.BasicRecommender):

    def __init__(self, dataModel, config):

        super(ReviewAttention, self).__init__(dataModel, config)


        self.user_reviewVectors = dataModel.user_reviewVectors

        self.name = 'ReviewPMF_CNN'

        self.numFactor = config['numFactor']
        self.rnn_unit_num = config['rnn_unit_num']
        self.rnn_layer_num = config['rnn_layer_num']
        self.rnn_dropout = config['rnn_dropout']
        self.rnn_cell = config['rnn_cell']
        self.wordVec_size = config['wordVec_size']
        self.lam1 = config['lam1']
        self.lam2 = config['lam2']
        self.addBias = config['addBias']
        self.with_review_intput = config['with_review_input']

        self.maxReviewLength = dataModel.maxReviewLength

        # placeholders
        self.u_id = tf.placeholder(tf.int32, [None, 1])
        self.i_id = tf.placeholder(tf.int32, [None, 1])
        self.r_label = tf.placeholder(tf.float32, [None, 1])
        self.u_items_ids = tf.placeholder(tf.int32, [None, 8])
        self.review = tf.placeholder(tf.float32, [None, 8, self.maxReviewLength, self.wordVec_size])

        self.u_items_ids_input = np.ones((1, 8), dtype=np.int16)
        self.review_input = np.ones((1, 8, self.maxReviewLength, self.wordVec_size), dtype=np.float32)

        # user/item embedding
        tf.set_random_seed(123)
        random.seed(123)
        self.userEmbedding = tf.Variable(tf.random_normal([self.numUser, self.numFactor], 0, 0.1))
        self.itemEmbedding = tf.Variable(tf.random_normal([self.numItem, self.numFactor], 0, 0.1))
        # self.wordEmbedding = tf.Variable(tf.random_uniform([self.numWord, self.numFactor], -0.1, 0.1))
        # RNN network
        self.rnn_network = self.RNN()
        self.rnn_outputWeights = tf.Variable(tf.random_normal([self.rnn_unit_num, self.numFactor], 0, 0.1))
        self.rnn_outputBias = tf.Variable(tf.random_normal([self.numFactor], 0, 0.1))
        # MLP network
        self.MLP_weights = {
            'h1': tf.Variable(tf.random_normal([self.numFactor * 9, self.numFactor], 0, 0.1),
                              name='i_bias', dtype=tf.float32),
            'b1': tf.Variable(tf.random_normal([self.numFactor], 0, 0.1),
                              name='i_bias', dtype=tf.float32),
        }
        # user/item bias
        if self.addBias:
            self.u_bias = tf.Variable(tf.random_uniform([self.numUser, 1], -0.1, 0.1))
            self.i_bias = tf.Variable(tf.random_uniform([self.numItem, 1], -0.1, 0.1))
            self.globalMean = np.mean(self.trainSet[:, 2:3])

        self.r_pred_withoutReview = None

    def buildModel(self):

        with tf.variable_scope(tf.get_variable_scope()) as scope:
            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])
            itemEmedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, self.i_id), [-1, self.numFactor])

            rnn_outputs = self.RNN_outputs()
            attention_outputs = self.attentionOutputs(userEmedding, itemEmedding)

            reviewOutputs = []
            for i in range(len(rnn_outputs)):
                reviewOutputs.append(rnn_outputs[i] * attention_outputs[i])
            reviewOutputs.append(userEmedding)

            userReviewEmbedding = tf.concat(reviewOutputs, 1)

            user_merge = tf.nn.relu(tf.matmul(userReviewEmbedding, self.MLP_weights['h1']) + self.MLP_weights['b1'])

            if self.goal == 'rating':
                raw_r_pred = tf.reduce_sum(tf.multiply(user_merge, itemEmedding), 1, keep_dims=True)
                if self.addBias:
                    u_bias = tf.reshape(tf.nn.embedding_lookup(self.u_bias, self.u_id), [-1, 1])
                    i_bias = tf.reshape(tf.nn.embedding_lookup(self.i_bias, self.i_id), [-1, 1])
                    self.r_pred = raw_r_pred + u_bias + i_bias + self.globalMean
                    self.r_pred_withoutReview = tf.reduce_sum(tf.multiply(userEmedding, itemEmedding), 1, keep_dims=True) + u_bias + i_bias + self.globalMean
                else:
                    self.r_pred = 1 + 4 * tf.nn.sigmoid(raw_r_pred)
                    self.r_pred_withoutReview = 1 + 4 * tf.nn.sigmoid(tf.reduce_sum(tf.multiply(userEmedding, itemEmedding), 1, keep_dims=True))

                ratingLoss = tf.reduce_sum(tf.square(self.r_label - self.r_pred))

            else: # ranking
                self.r_pred = tf.reduce_sum(tf.multiply(user_merge, itemEmedding), 1, keep_dims=True)
                self.r_pred_withoutReview = tf.nn.sigmoid(tf.reduce_sum(tf.multiply(userEmedding, itemEmedding), 1, keep_dims=True))
                ratingLoss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=self.r_label, logits=self.r_pred))


        reg_loss = self.lam1 * tf.nn.l2_loss(userEmedding) \
                   + self.lam1 * tf.nn.l2_loss(itemEmedding)\
                   + self.lam2 * tf.nn.l2_loss(self.MLP_weights['h1'])\
                   + self.lam2 * tf.nn.l2_loss(self.MLP_weights['b1'])

        if self.addBias:
            bias_reg_loss = self.lam1 * tf.nn.l2_loss(self.u_bias) \
                       + self.lam1 * tf.nn.l2_loss(self.i_bias)
            reg_loss += bias_reg_loss

        self.cost = ratingLoss + reg_loss

    def trainEachBatch(self, epochId, batchId):
        batch_u, batch_v, batch_r, batch_u_items, batch_u_reviews = self.getTrainData(batchId)
        # print("-----------------------------------------------------")
        # print("user: " + str(self.userIdxToUserId[batch_u[0][0]]))
        # print("item: " + str(self.itemIdxToItemId[batch_v[0][0]]))
        # print("review: " + str(batch_u_reviews.shape))
        self.optimizer.run(feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
            self.r_label: batch_r,
            self.u_items_ids: batch_u_items,
            self.review: batch_u_reviews,
        })
        loss = self.cost.eval(feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
            self.r_label: batch_r,
            self.u_items_ids: batch_u_items,
            self.review: batch_u_reviews,
        })


        if batchId == 0:
            self.logger.info("----------------------------------------------------------------------")
            self.logger.info(
                "batchId: %d epoch %d/%d   loss: %.4f" % (batchId, epochId, self.maxIter, loss))
            if self.goal == 'ranking':
                self.evaluateRanking(epochId, batchId)
            else:
                self.evaluateRating(epochId, batchId)

    def length(self, review):
        used = tf.sign(tf.reduce_max(tf.abs(review), reduction_indices=2))
        length = tf.reduce_sum(used, reduction_indices=1)
        length = tf.cast(length, tf.int32)
        return length

    def RNN(self):
        num_units = self.rnn_unit_num
        num_layers = self.rnn_layer_num

        cells = []
        for _ in range(num_layers):
            if self.rnn_cell == 'GRU':
                cell = tf.contrib.rnn.GRUCell(num_units)  # Or LSTMCell(num_units)
            else:
                cell = tf.contrib.rnn.LSTMCell(num_units)
            if self.rnn_dropout == 1:
                pass
            else:
                cell = tf.contrib.rnn.DropoutWrapper(
                    cell, output_keep_prob=1.0 - self.rnn_dropout)
            cells.append(cell)
        return tf.contrib.rnn.MultiRNNCell(cells)

    def RNN_outputs(self):
        # Batch size x time steps x features.
        reviews = []
        rele_outputs = []
        for review in tf.split(self.review, [1, 1, 1, 1, 1, 1, 1, 1], 1):
            reviews.append(tf.reshape(review, [-1, self.maxReviewLength, self.wordVec_size]))
        # reviews.extend(tf.reshape(, [-1, self.maxReviewLength, self.wordVec_size]))

        for review in reviews:
            length = self.length(review)
            output, _ = tf.nn.dynamic_rnn(
                self.rnn_network,
                review,
                dtype=tf.float32,
                sequence_length=length,
            )
            last = self.last_relevant(output, length)
            rele_output = tf.nn.sigmoid(tf.matmul(last, self.rnn_outputWeights) + self.rnn_outputBias)
            rele_outputs.append(rele_output)
        return rele_outputs

    def attentionOutputs(self, userEmbedding, curr_itemEmbedding):
        itemIds = []
        attentionValues = None
        attentionOutputs = []

        itemIds.extend(tf.split(self.u_items_ids, [1, 1, 1, 1, 1, 1, 1, 1], 1))

        for itemId in itemIds:
            itemEmbedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, itemId), [-1, self.numFactor])
            itemCorr = tf.multiply(curr_itemEmbedding, itemEmbedding)
            attentionValue = tf.reduce_sum(tf.multiply(userEmbedding, itemCorr), 1, keep_dims=True)
            if attentionValues is None:
                attentionValues = attentionValue
            else:
                attentionValues = tf.concat([attentionValues, attentionValue], 1)
        attentionValues = tf.nn.softmax(attentionValues)
        attentionOutputs.extend(tf.split(attentionValues, [1, 1, 1, 1, 1, 1, 1, 1], 1))

        return attentionOutputs

    def last_relevant(self, output, length):
        batch_size = tf.shape(output)[0]
        max_length = tf.shape(output)[1]
        out_size = int(output.get_shape()[2])
        index = tf.range(0, batch_size) * max_length + (length - 1)
        flat = tf.reshape(output, [-1, out_size])
        relevant = tf.gather(flat, index)
        return relevant

    def getTrainData(self, batchId):
        # compute start and end
        start = batchId * self.trainBatchSize
        end = start + self.trainBatchSize
        if end > self.trainSize:
            end = self.trainSize

        batch_u = self.trainSet[start:end, 0:1].astype(np.int32)
        batch_v = self.trainSet[start:end, 1:2].astype(np.int32)
        batch_r = self.trainSet[start:end, 2:3]

        reviewBatch = []
        user_items_batch = []
        for userId in batch_u:
            userId = userId[0]
            reviewVectors = self.user_reviewVectors[userId]
            reviewBatch.append(reviewVectors)

            items = self.user_items_train[userId]
            user_items_batch.append(items)

        batch_u = np.array(batch_u)
        batch_v = np.array(batch_v)
        batch_r = np.array(batch_r)
        user_items_batch = np.array(user_items_batch)
        reviewBatch = np.array(reviewBatch)

        return batch_u, batch_v, batch_r, user_items_batch, reviewBatch

    def getRatingPredictions(self):
        batch_u = self.testSet[:, 0:1].astype(np.int32)
        batch_v = self.testSet[:, 1:2].astype(np.int32)

        # with review input
        if self.with_review_intput:
            reviewBatch = []
            user_items_batch = []
            for userId in batch_u:
                userId = userId[0]
                reviewVectors = self.user_reviewVectors[userId]
                reviewBatch.append(reviewVectors)

                items = self.user_items_train[userId]
                user_items_batch.append(items)

            predList = self.sess.run(self.r_pred, feed_dict={
                self.u_id: batch_u,
                self.i_id: batch_v,
                self.u_items_ids: np.array(user_items_batch),
                self.review: np.array(reviewBatch),
            })
        # without review input
        else:
            predList = self.sess.run(self.r_pred_withoutReview, feed_dict={
                self.u_id: batch_u,
                self.i_id: batch_v,
                self.u_items_ids: self.u_items_ids_input,
                self.review: self.review_input,
            })

        for i in range(len(predList)):
            if predList[i][0] > 5:
                predList[i][0] = 5
            if predList[i][0] < 1:
                predList[i][0] = 1
        return predList


    def getPredList_ByUserIdx(self, userIdx):

        # build test batch
        testBatch_user = []

        for itemIdx in self.evalItemsForEachUser[userIdx]:
            testBatch_user.append([userIdx, itemIdx, 1.0])

        batch_u = np.array(testBatch_user)[:, 0:1]
        batch_v = np.array(testBatch_user)[:, 1:2]
        # with review input
        if self.with_review_intput:
            batch_u_reviews = [self.user_reviewVectors[userIdx] for i in range(len(testBatch_user))]
            batch_u_items = [self.user_items_train[userIdx] for i in range(len(testBatch_user))]

            batch_u_reviews = np.array(batch_u_reviews)
            batch_u_items = np.array(batch_u_items)

            predList = self.sess.run(self.r_pred, feed_dict={
                self.u_id: batch_u,
                self.i_id: batch_v,
                self.u_items_ids: batch_u_items,
                self.review: batch_u_reviews,
            })

        else:
            predList = self.sess.run(self.r_pred_withoutReview, feed_dict={
                self.u_id: batch_u,
                self.i_id: batch_v,
                self.u_items_ids: self.u_items_ids_input,
                self.review: self.review_input,
            })
        recommendList = {}
        for i in range(len(testBatch_user)):
            recommendList[batch_v[i][0]] = predList[i][0]

        sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
        # print("user " + str(userIdx) + "/" + str(self.numUser-1) + " finished")

        return sorted_RecItemList












