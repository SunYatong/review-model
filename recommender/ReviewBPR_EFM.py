import numpy as np
import tensorflow as tf
import random
import time

from recommender.ReviewBPR_NEW import ReviewBPR_NEW



class ReviewBPR_EFM(ReviewBPR_NEW):
    def __init__(self, dataModel, config):

        super(ReviewBPR_EFM, self).__init__(dataModel, config)

    def buildModel(self):

        with tf.variable_scope(tf.get_variable_scope()) as scope:
            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])
            itemEmedding_i = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, self.i_id), [-1, self.numFactor])
            itemEmedding_j = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, self.j_id), [-1, self.numFactor])

            item_review_embed_i = self.review_network.get_single_output(self.positive_item_review_input)
            item_review_embed_j = self.review_network.get_single_output(self.negative_item_review_input)

            component_outputs = self.review_network.get_outputs(self.review_input)

            if self.attention_version == 'old':
                attention_outputs_i, item_embeddings = self.attentionOutputs(userEmedding, itemEmedding_i, flag='p')
                attention_outputs_j, item_embeddings = self.attentionOutputs(userEmedding, itemEmedding_j, flag='n')
            elif self.attention_version == 'new':
                attention_outputs_i, item_embeddings = self.new_attention_output(userEmedding, itemEmedding_i, item_review_embed_i,
                                                                                 component_outputs, 'p')
                attention_outputs_j, item_embeddings = self.new_attention_output(userEmedding, itemEmedding_j, item_review_embed_j,
                                                                                 component_outputs, 'n')
            else:
                attention_outputs_i, item_embeddings = self.no_attentionOutputs('p')
                attention_outputs_j, item_embeddings = self.no_attentionOutputs('n')

            item_summary_i = None
            item_summary_j = None
            review_summary_i = None
            review_summary_j = None
            for index in range(len(item_embeddings)):
                if item_summary_i is None and item_summary_j is None and review_summary_i is None and review_summary_j is None:
                    item_summary_i = (item_embeddings[index]) * attention_outputs_i[index]
                    item_summary_j = (item_embeddings[index]) * attention_outputs_j[index]
                    review_summary_i = component_outputs[index] * attention_outputs_i[index]
                    review_summary_j = component_outputs[index] * attention_outputs_j[index]
                else:
                    item_summary_i += (item_embeddings[index]) * attention_outputs_i[index]
                    item_summary_j += (item_embeddings[index]) * attention_outputs_j[index]
                    review_summary_i += component_outputs[index] * attention_outputs_i[index]
                    review_summary_j += component_outputs[index] * attention_outputs_j[index]

            pred_i = tf.reduce_sum(tf.multiply(userEmedding + item_summary_i, itemEmedding_i), 1, keep_dims=True) + tf.reduce_sum(tf.multiply(review_summary_i, item_review_embed_i), 1, keep_dims=True)
            pred_j = tf.reduce_sum(tf.multiply(userEmedding + item_summary_j, itemEmedding_j), 1, keep_dims=True) + tf.reduce_sum(tf.multiply(review_summary_j, item_review_embed_j), 1, keep_dims=True)

            predDiff = pred_i - pred_j

            self.rating_loss = - tf.reduce_sum(tf.log(tf.sigmoid(predDiff)))

            self.r_pred = pred_i

        self.factor_l2_loss = self.factor_lambda * tf.nn.l2_loss(userEmedding) \
                              + self.factor_lambda * tf.nn.l2_loss(itemEmedding_i) \
                              + self.factor_lambda * tf.nn.l2_loss(itemEmedding_j) \

        self.review_l2_loss = self.review_network.get_l2_loss()

        self.cost = self.rating_loss + self.factor_l2_loss + self.review_l2_loss

        if self.attention_version != 'old':
            self.cost += self.atten_reg

        self.u_id_print = tf.Print(input_=self.u_id, data=[self.u_id], message="user_idx", summarize=1)
        self.positive_i_print = tf.Print(input_=self.i_id, data=[self.i_id], message="positive_item_idx", summarize=1)
        self.negative_j_print = tf.Print(input_=self.j_id, data=[self.j_id], message="negative_item_idx", summarize=1)
        self.item_ids_print = tf.Print(input_=self.u_items_ids, data=[self.u_items_ids], message="hist_items_idx",
                                       summarize=8)


    def attentionOutputs(self, userEmbedding, curr_itemEmbedding, flag):
        itemIds = []
        attentionValues = None
        attentionOutputs = []

        split_list = [1] * self.item_pad_num
        itemIds.extend(tf.split(self.u_items_ids, split_list, 1))

        for itemId in itemIds:
            itemEmbedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, itemId), [-1, self.numFactor])
            itemCorr = tf.multiply(curr_itemEmbedding, itemEmbedding)
            attentionValue = tf.reduce_sum(tf.multiply(userEmbedding, itemCorr), 1, keep_dims=True)
            if attentionValues is None:
                attentionValues = attentionValue
            else:
                attentionValues = tf.concat([attentionValues, attentionValue], 1)

        if flag == "p":
            attentionValues = self.softmask(mask=self.u_pos_items_ids_mask, attentions=attentionValues)
            self.atten_i_print = tf.Print(attentionValues, [attentionValues], message="attention_i", summarize=self.item_pad_num)
        else:
            attentionValues = tf.nn.softmax(attentionValues)
            self.atten_j_print = tf.Print(attentionValues, [attentionValues], message="attention_j", summarize=self.item_pad_num)

        attentionOutputs.extend(tf.split(attentionValues, split_list, 1))

        return attentionOutputs

    def new_attention_output(self, user_embedding, curr_item_embedding, curr_item_review, reviewEmbeddings, flag):
        attention_output_list = []
        item_embeddings = []
        with tf.variable_scope("attention_weifhts", reuse=True):
            user_W = tf.get_variable(name="user_W", shape=[self.numFactor, self.numFactor // 2], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())
            item_W = tf.get_variable(name="item_W", shape=[self.numFactor, self.numFactor // 2], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())
            review_W = tf.get_variable(name="review_W", shape=[self.numFactor, self.numFactor // 2], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())
            embed_bias = tf.get_variable(name="embed_bias", dtype=tf.float32,
                                        initializer=tf.constant(0.0, shape=[self.numFactor // 2]))
            summary_W = tf.get_variable(name="summary_W", shape=[self.numFactor // 2, 1], dtype=tf.float32,
                                        initializer=tf.contrib.layers.xavier_initializer())
            summary_b = tf.get_variable(name="summary_b", dtype=tf.float32,
                                        initializer=tf.constant(0.0))

            user_atten = tf.matmul(user_embedding, user_W)
            curr_item_atten = tf.matmul(curr_item_embedding, item_W)
            curr_item_review_atten = tf.matmul(curr_item_review, review_W)
            attention_outputs = None

            split_list = [1] * self.item_pad_num
            itemIds = tf.split(self.u_items_ids, split_list, 1)
            for i in range(self.item_pad_num):
                hist_item_embed = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, itemIds[i]), [-1, self.numFactor])
                item_embeddings.append(hist_item_embed)
                hist_item_atten = tf.matmul(hist_item_embed, item_W)
                review_atten = tf.matmul(reviewEmbeddings[i], review_W)
                self.review_atten_print[i] = tf.Print(review_atten, [review_atten], message="review_atten_print%d"%(i), summarize=self.numFactor // 2)
                atten_embed = tf.nn.relu(user_atten + curr_item_atten + hist_item_atten + review_atten + curr_item_review_atten + embed_bias)

                atten_output = tf.matmul(atten_embed, summary_W) + summary_b

                if attention_outputs == None:
                    attention_outputs = atten_output
                else:
                    attention_outputs = tf.concat([attention_outputs, atten_output], 1)

            if flag == "p":
                # self.atten_i_print = tf.Print(attention_outputs, [attention_outputs], message="attention_i", summarize=self.item_pad_num)
                attentionValues = self.softmask(mask=self.u_pos_items_ids_mask, attentions=attention_outputs)
                self.atten_i_print = tf.Print(attentionValues, [attentionValues], message="attention_i", summarize=self.item_pad_num)
            else:
                # self.atten_j_print = tf.Print(attention_outputs, [attention_outputs], message="attention_j", summarize=self.item_pad_num)
                attentionValues = self.softmask(mask=self.u_pos_items_ids_mask, attentions=attention_outputs)
                self.atten_j_print = tf.Print(attentionValues, [attentionValues], message="attention_j", summarize=self.item_pad_num)

            attention_output_list.extend(tf.split(attentionValues, split_list, 1))

            return attention_output_list, item_embeddings



















