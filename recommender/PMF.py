import numpy as np
import tensorflow as tf
import random
import recommender.BasicRcommender
import time

class PMFRecommender(recommender.BasicRcommender.BasicRecommender):

    def __init__(self, dataModel, config):

        super(PMFRecommender, self).__init__(dataModel, config)


        self.user_reviewVectors = dataModel.user_reviewVectors

        self.name = 'PMFRecommender'
        self.negative = config['negative_sample']

        self.numFactor = config['numFactor']
        self.lam1 = config['lam1']

        # placeholders
        self.u_id = tf.placeholder(tf.int32, [None, 1])
        self.i_id = tf.placeholder(tf.int32, [None, 1])
        self.r_label = tf.placeholder(tf.float32, [None, 1])

        # user/item embedding
        tf.set_random_seed(123)
        random.seed(123)
        self.userEmbedding = tf.Variable(tf.random_normal([self.numUser, self.numFactor], 0, 0.1))
        self.itemEmbedding = tf.Variable(tf.random_normal([self.numItem, self.numFactor], 0, 0.1))

    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:
            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])
            itemEmedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, self.i_id), [-1, self.numFactor])

            l2_norm = self.lam1 * tf.nn.l2_loss(userEmedding) \
                      + self.lam1 * tf.nn.l2_loss(itemEmedding) \

            if self.goal == 'ranking':
                self.r_pred = tf.reduce_sum(tf.multiply(userEmedding, itemEmedding), 1, keep_dims=True)

                # ratingLoss = tf.nn.l2_loss(self.r_label-self.r_pred)
                ratingLoss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=self.r_label, logits=self.r_pred))
                # ratingLoss = tf.reduce_mean(tf.losses.log_loss(labels=self.r_label, predictions=tf.nn.sigmoid(self.r_pred)))
            else:
                self.r_pred = tf.reduce_sum(tf.multiply(userEmedding, itemEmedding), 1, keep_dims=True)
                ratingLoss = tf.reduce_sum(tf.square(self.r_pred - self.r_label))

        self.cost = ratingLoss + l2_norm

    def trainEachBatch(self, epochId, batchId):
        batch_u, batch_v, batch_r = self.getTrainData(batchId)
        self.optimizer.run(feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
            self.r_label: batch_r
        })
        loss = self.cost.eval(feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
            self.r_label: batch_r
        })

        if batchId == 0:
            self.logger.info("----------------------------------------------------------------------")
            self.logger.info(
                "batchId: %d epoch %d/%d   loss: %.4f" % (batchId, epochId, self.maxIter, loss))
            if self.goal == 'ranking':
                self.evaluateRanking(epochId, batchId)
            else:
                self.evaluateRating(epochId, batchId)
        return loss



    def getTrainData(self, batchId):
        # compute start and end
        start = batchId * self.trainBatchSize
        end = start + self.trainBatchSize
        if end > self.trainSize:
            end = self.trainSize
        batch_u = self.trainSet[start:end, 0:1].astype(np.int32)
        batch_v = self.trainSet[start:end, 1:2].astype(np.int32)
        batch_r = self.trainSet[start:end, 2:3]

        if self.goal == 'ranking' and self.negative > 0:
            rankBatch = self.trainSet[start:end]
            for user in batch_u:
                userIdx = user[0]
                itemsInTrain = self.user_items_train[userIdx]
                for negativeNum in range(self.trainBatchSize * self.negative):
                    newItemIdx = random.randint(0, self.numItem-1)
                    while newItemIdx in itemsInTrain:
                        newItemIdx = random.randint(0, self.numItem-1)
                    rankBatch = np.append(rankBatch, [[userIdx, newItemIdx, 0.0]], 0)
            batch_u = rankBatch[:, 0:1].astype(np.int32)
            batch_v = rankBatch[:, 1:2].astype(np.int32)
            batch_r = rankBatch[:, 2:3]

        # print(self.userEmbedding.eval())

        return batch_u, batch_v, batch_r


    def getRatingPredictions(self):
        batch_u = self.testSet[:, 0:1].astype(np.int32)
        batch_v = self.testSet[:, 1:2].astype(np.int32)

        predList = self.r_pred.eval(feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v
        })


        for i in range(len(predList)):
            if predList[i][0] > 5:
                predList[i][0] = 5
            if predList[i][0] < 1:
                predList[i][0] = 1

        # print(predList)
        return predList

    def getPredList_ByUserIdxList(self, user_idices):
        end0 = time.time()
        # build test batch
        testBatch_user = []

        for i in range(len(user_idices)):
            userIdx = user_idices[i]
            for itemIdx in self.evalItemsForEachUser[userIdx]:
                testBatch_user.append([userIdx, itemIdx, 1.0])

        batch_u = np.array(testBatch_user)[:, 0:1].astype(np.int32)
        batch_v = np.array(testBatch_user)[:, 1:2].astype(np.int32)
        end1 = time.time()

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
        })
        end2 = time.time()

        output_lists = []
        for i in range(len(user_idices)):
            recommendList = {}
            start = i * 100
            end = start + 100
            for j in range(start, end):
                recommendList[batch_v[j][0]] = predList[j][0]
            sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
            output_lists.append(sorted_RecItemList)
        end3 = time.time()

        return output_lists, end1 - end0, end2 - end1, end3 - end2


    def getPredList_ByUserIdx(self, userIdx):
        # build test batch
        testBatch_user = []

        for itemIdx in self.evalItemsForEachUser[userIdx]:
            testBatch_user.append([userIdx, itemIdx, 1.0])

        batch_u = np.array(testBatch_user)[:, 0:1].astype(np.int32)
        batch_v = np.array(testBatch_user)[:, 1:2].astype(np.int32)

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
        })

        recommendList = {}
        for i in range(len(testBatch_user)):
            recommendList[batch_v[i][0]] = predList[i][0]

        sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]

        return sorted_RecItemList












