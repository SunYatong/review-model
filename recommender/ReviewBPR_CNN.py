import tensorflow as tf
from recommender.ReviewRecommender import ReviewRecommender
from component.Conv_Pool import CNN_Pool_Compoment

class ReviewBPR_CNN(ReviewRecommender):

    def __init__(self, dataModel, config):

        super(ReviewBPR_CNN, self).__init__(dataModel, config)

        self.name = 'ReviewBPR_CNN'

        self.cnn_lambda = config['cnn_lambda']
        self.filter_sizes = config['filters_sizes']
        self.num_filters = config['num_filters']

        # cnn parameters
        self.review_network = CNN_Pool_Compoment(
            filter_num=self.num_filters,
            filter_sizes=self.filter_sizes,
            output_size=self.numFactor,
            wordvec_size=self.wordVec_size,
            max_review_length=self.maxReviewLength,
            word_matrix=self.word_embedding_matrix,
            review_wordId_print=self.review_wordId_print,
            review_input_print=self.review_input_print,
            cnn_lambda=self.cnn_lambda,
            dropout_keep_prob=self.dropout_keep_prob,
            component_raw_output=self.component_raw_output,
            item_pad_num=self.item_pad_num
        )






