import numpy as np
import tensorflow as tf
import random
from recommender.CapSeq_Basic import CapSeqBasic
import time
from component.Conv import CNN_Compoment
from component.Capsule import Capsule_Component
from component.MLP import MLP

class CapSeqRawRecommender(CapSeqBasic):

    def __init__(self, dataModel, config):

        super(CapSeqRawRecommender, self).__init__(dataModel, config)

        self.name = 'CapSeq_raw-no-user'

        # Conv-Capsule Layers
        self.hor_convs = []
        self.hor_pattern_capsules = []
        self.user_embeddings = []
        for i in range(self.input_length):
            filter_height = i + 1
            conv = CNN_Compoment(
                filter_num=self.filter_num,
                filter_height=filter_height,
                filter_width=self.numFactor,
                name='hor-' + str(filter_height)
            )
            capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.filter_num,
                num_caps_j=self.capsule_num,
                in_vec_len=self.input_length - filter_height + 1,
                out_vec_len=self.input_length - filter_height + 1,
                user_vec_len=self.numFactor,
                user_bias=True,
                T=self.dynamic_routing_iter,
                name=str('pattern-' + str(filter_height))
            )
            self.hor_convs.append(conv)
            self.hor_pattern_capsules.append(capsule)

        # time capsule
        self.time_capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.input_length,
                num_caps_j=self.capsule_num,
                in_vec_len=self.numFactor,
                out_vec_len=self.numFactor,
                user_vec_len=self.numFactor,
                user_bias=True,
                T=self.dynamic_routing_iter,
                name='time'
            )

        self.concate_size = (((
                              self.input_length + 1) * self.input_length) / 2 + self.numFactor) * self.capsule_num + self.numFactor
        self.fc_dim = [self.concate_size] + config['item_fc_dim']
        self.mlp = MLP(self.fc_dim, dropout_keep=self.dropout_keep_placeholder)

    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:

            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])

            input_image = tf.nn.embedding_lookup(self.itemEmbedding, self.input_seq)
            input_image = tf.reshape(input_image, [-1, self.input_length, self.numFactor])
            input_image = tf.expand_dims(input_image, -1)
            # assert input_image.get_shape() == [-1, self.input_length, self.numFactor, 1]

            embeddings = []
            for i in range(self.input_length):
                filter_height = i + 1
                conv = self.hor_convs[i].get_output(input_image)
                # assert conv.get_shape() == [-1, self.input_length-filter_height+1, 1, self.filter_num]

                conv = tf.transpose(conv, perm=[0, 3, 1, 2])
                # assert conv.get_shape() == [-1, self.filter_num, self.input_length-filter_height+1, 1]

                # assert conv.get_shape() == [-1, self.input_length - filter_height + 1, self.filter_num, 1]
                # raw capsule output shape = [batch_size, num_j_capsule, output_vec_len, 1]
                output = self.hor_pattern_capsules[i].get_output(conv, None)
                # assert output.get_shape() == [-1, self.input_length - filter_height + 1]
                output = tf.reshape(output, [-1, self.capsule_num * (self.input_length - filter_height + 1)])
                embeddings.append(output)

            # raw capsule output shape = [batch_size, num_j_capsule, output_vec_len, 1]
            time_output = self.time_capsule.get_output(input_image, None)
            time_output = tf.reshape(time_output, [-1, self.capsule_num * self.numFactor])
            # assert time_output.get_shape() == [-1, capsule_num * self.numFactor]
            embeddings.append(time_output)
            embeddings.append(userEmedding)

            concate_vec = tf.concat(values=embeddings, axis=1)
            self.cap_output = tf.Print(input_=concate_vec, data=[concate_vec], message="cap_output",
                                                   summarize=10)
            # assert concate_vec.get_shape() == [-1, self.concate_size]
            merged_embed = self.mlp.get_output(feature_input=concate_vec)
            # assert merged_embed.get_shape() == [-1, self.numFactor]

            pos_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_pos,
                tar_length=self.target_length,
            )
            neg_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_neg,
                tar_length=self.target_length
            )

            bpr_loss = - tf.reduce_sum(tf.log(tf.sigmoid(pos_preds - neg_preds)))
            l2_loss = self.factor_lambda * (tf.nn.l2_loss(self.userEmbedding) + tf.nn.l2_loss(self.itemEmbedding))

            self.cost = bpr_loss + l2_loss

            self.r_pred = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.pred_seq,
                tar_length=100
            )
