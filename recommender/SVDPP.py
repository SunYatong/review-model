import numpy as np
import tensorflow as tf
import random
import time
from recommender.ReviewRecommender import ReviewRecommender
from component.RNN import RNN_Compoment
from component.Conv_Pool import CNN_Pool_Compoment


class SVDPP(ReviewRecommender):
    def __init__(self, dataModel, config):

        super(SVDPP, self).__init__(dataModel, config)

        self.name = 'SVDPP'
        self.component_type = config['component_type']
        self.item_reviewVectors = dataModel.item_reviewVectors
        self.add_relu = config['add_relu']
        self.feature_atten = config['feature_atten']

        # self.saver = tf.train.Saver(self.weights)
        self.atten_level = config['atten_level']

    def buildModel(self):

        with tf.variable_scope(tf.get_variable_scope()) as scope:
            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])
            itemEmedding_i = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, self.i_id), [-1, self.numFactor])
            itemEmedding_j = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, self.j_id), [-1, self.numFactor])


            attention_outputs_i, item_embeddings = self.new_attention_output(userEmedding,
                                                                                 itemEmedding_i,
                                                                                  'p')

            attention_outputs_j = attention_outputs_i
            # if self.attention_version == 'old':
            #     attention_outputs_i, item_embeddings = self.attentionOutputs(userEmedding,
            #                                                                      itemEmedding_i,
            #                                                                      item_review_embed_i,
            #                                                                      component_outputs, 'p')
            #     attention_outputs_j, item_embeddings = self.attentionOutputs(userEmedding,
            #                                                                      itemEmedding_j,
            #                                                                      item_review_embed_j,
            #                                                                      component_outputs, 'n')
            # elif self.attention_version == 'new':
            #     attention_outputs_i, item_embeddings = self.new_attention_output(userEmedding,
            #                                                                      itemEmedding_i,
            #                                                                      item_review_embed_i,
            #                                                                      component_outputs, 'p')
            #     attention_outputs_j, item_embeddings = self.new_attention_output(userEmedding,
            #                                                                      itemEmedding_j,
            #                                                                      item_review_embed_j,
            #                                                                      component_outputs, 'n')
            # else:
            #     attention_outputs_i, item_embeddings = self.no_attentionOutputs('p')
            #     attention_outputs_j, item_embeddings = self.no_attentionOutputs('n')

            summary_i = None
            summary_j = None
            for index in range(len(item_embeddings)):
                if summary_i is None and summary_j is None:
                    summary_i = (item_embeddings[index]) * attention_outputs_i[index]
                    summary_j = (item_embeddings[index]) * attention_outputs_j[index]
                else:
                    summary_i += (item_embeddings[index]) * attention_outputs_i[index]
                    summary_j += (item_embeddings[index]) * attention_outputs_j[index]


            if self.add_relu:
                pred_factor_i = tf.multiply(tf.nn.relu(userEmedding + summary_i), tf.nn.relu(itemEmedding_i))
                pred_factor_j = tf.multiply(tf.nn.relu(userEmedding + summary_j), tf.nn.relu(itemEmedding_j))
            else:
                # a = userEmedding + summary_i
                a = userEmedding * summary_i
                # b = itemEmedding_i + item_review_embed_i
                b = itemEmedding_i

                pred_factor_i = tf.multiply(a, b)
                pred_factor_j = tf.multiply(userEmedding + summary_j, itemEmedding_j)

            pred_i = tf.reduce_sum(pred_factor_i, 1, keep_dims=True)
            pred_j = tf.reduce_sum(pred_factor_j, 1, keep_dims=True)

            predDiff = pred_i - pred_j

            self.rating_loss = - tf.reduce_sum(tf.log(tf.sigmoid(predDiff)))

            self.r_pred = pred_i

        self.factor_l2_loss = self.factor_lambda * tf.nn.l2_loss(userEmedding) \
                              + self.factor_lambda * tf.nn.l2_loss(itemEmedding_i) \
                              + self.factor_lambda * tf.nn.l2_loss(itemEmedding_j) \

        self.cost = self.rating_loss + self.factor_l2_loss

        if self.attention_version != 'old':
            self.cost += self.atten_reg

        self.u_id_print = tf.Print(input_=self.u_id, data=[self.u_id], message="user_idx", summarize=1)
        self.positive_i_print = tf.Print(input_=self.i_id, data=[self.i_id], message="positive_item_idx", summarize=1)
        self.negative_j_print = tf.Print(input_=self.j_id, data=[self.j_id], message="negative_item_idx", summarize=1)
        self.item_ids_print = tf.Print(input_=self.u_items_ids, data=[self.u_items_ids], message="hist_items_idx",
                                       summarize=8)

    def getTrainData(self, batchId):
        start = time.time()

        user_batch = []
        pos_item_batch = []
        neg_item_batch = []

        user_items_batch = []
        user_items_mask_batch = []

        for i in range(self.trainBatchSize):
            userIdx = random.randint(0, self.numUser - 1)
            positiveItems = self.user_items_train[userIdx]
            positiveItems_paded = self.user_items_train_paded[userIdx]

            positiveItems_mask = list(self.user_review_mask[userIdx])

            random_idx = random.randint(0, len(positiveItems) - 1)
            positiveItems_mask[random_idx] = False
            positiveItemIdx = positiveItems[random_idx]
            negativeItemIdx = random.randint(0, self.numItem - 1)
            while negativeItemIdx in positiveItems:
                negativeItemIdx = random.randint(0, self.numItem - 1)


            user_batch.append(userIdx)
            pos_item_batch.append(positiveItemIdx)
            neg_item_batch.append(negativeItemIdx)
            user_items_batch.append(positiveItems_paded)
            user_items_mask_batch.append(positiveItems_mask)


        batch_u = np.array(user_batch).reshape((self.trainBatchSize, 1))
        batch_i = np.array(pos_item_batch).reshape((self.trainBatchSize, 1))
        batch_j = np.array(neg_item_batch).reshape((self.trainBatchSize, 1))
        user_items_batch = np.array(user_items_batch)

        user_items_mask_batch = np.array(user_items_mask_batch)

        end = time.time()

        return batch_u, batch_i, batch_j, user_items_batch, user_items_mask_batch, str((end - start))

    def trainEachBatch(self, epochId, batchId):
        start = time.time()
        print_flag = 1
        batch_u, batch_v, batch_j, batch_u_items, batch_u_items_bask, time_cost = self.getTrainData(batchId)

        _, ratingLoss, factor_l2_loss = self.sess.run(
            fetches=[
                self.optimizer, self.rating_loss, self.factor_l2_loss],
            feed_dict={
                self.u_id: batch_u,
                self.i_id: batch_v,
                self.j_id: batch_j,
                self.u_items_ids: batch_u_items,
                self.dropout_keep_prob: self.dropout_keep_prob_input,
                self.u_pos_items_ids_mask: batch_u_items_bask,
                self.batch_num: self.trainBatchSize,

            })

        # self.optimizer.run(feed_dict={
        #     self.u_id: batch_u,
        #     self.i_id: batch_v,
        #     self.j_id: batch_j,
        #     self.u_items_ids: batch_u_items,
        #     self.review_input: batch_u_reviews,
        #     self.dropout_keep_prob: self.dropout_keep_prob_input,
        #     self.u_pos_items_ids_mask: batch_u_items_bask,
        #     self.batch_num: self.trainBatchSize,
        #     self.positive_item_review_input: pos_item_review,
        #     self.negative_item_review_input: neg_item_review,
        # })
        # ratingLoss = self.rating_loss.eval(feed_dict={
        #     self.u_id: batch_u,
        #     self.i_id: batch_v,
        #     self.j_id: batch_j,
        #     self.u_items_ids: batch_u_items,
        #     self.review_input: batch_u_reviews,
        #     self.dropout_keep_prob: self.dropout_keep_prob_input,
        #     self.u_pos_items_ids_mask: batch_u_items_bask,
        #     self.batch_num: self.trainBatchSize,
        #     self.positive_item_review_input: pos_item_review,
        #     self.negative_item_review_input: neg_item_review,
        # })
        # factor_l2_loss = self.factor_l2_loss.eval(feed_dict={
        #     self.u_id: batch_u,
        #     self.i_id: batch_v,
        #     self.j_id: batch_j,
        #     self.u_items_ids: batch_u_items,
        #     self.review_input: batch_u_reviews,
        #     self.dropout_keep_prob: self.dropout_keep_prob_input,
        #     self.u_pos_items_ids_mask: batch_u_items_bask,
        #     self.batch_num: self.trainBatchSize,
        #     self.positive_item_review_input: pos_item_review,
        #     self.negative_item_review_input: neg_item_review,
        # })
        # review_l2_loss = self.review_l2_loss.eval(feed_dict={
        #     self.u_id: batch_u,
        #     self.i_id: batch_v,
        #     self.j_id: batch_j,
        #     self.u_items_ids: batch_u_items,
        #     self.review_input: batch_u_reviews,
        #     self.dropout_keep_prob: self.dropout_keep_prob_input,
        #     self.u_pos_items_ids_mask: batch_u_items_bask,
        #     self.batch_num: self.trainBatchSize,
        #     self.positive_item_review_input: pos_item_review,
        #     self.negative_item_review_input: neg_item_review,
        # })
        loss = ratingLoss + factor_l2_loss

        if batchId == 0 and print_flag == 1:
            # self.u_id_print.eval(feed_dict={
            #     self.u_id: batch_u,
            # })
            # self.positive_i_print.eval(feed_dict={
            #     self.i_id: batch_v,
            # })
            # self.item_ids_print.eval(feed_dict={
            #     self.u_items_ids: batch_u_items,
            # })
            # self.u_pos_items_ids_mask_print.eval(feed_dict={
            #     self.u_pos_items_ids_mask: batch_u_items_bask
            # })
            # print detail info of review
            if self.verbose:
                for i in range(self.item_pad_num):
                    self.review_wordId_print[i].eval(feed_dict={
                        self.u_id: batch_u,
                        self.i_id: batch_v,
                        self.j_id: batch_j,
                        self.u_items_ids: batch_u_items,
                        self.dropout_keep_prob: self.dropout_keep_prob_input,
                        self.u_pos_items_ids_mask: batch_u_items_bask,
                        self.batch_num: self.trainBatchSize,

                    })
                for i in range(self.item_pad_num):
                    self.review_input_print[i].eval(feed_dict={
                        self.u_id: batch_u,
                        self.i_id: batch_v,
                        self.j_id: batch_j,
                        self.u_items_ids: batch_u_items,
                        self.dropout_keep_prob: self.dropout_keep_prob_input,
                        self.u_pos_items_ids_mask: batch_u_items_bask,
                        self.batch_num: self.trainBatchSize,

                    })
                # for i in range(self.item_pad_num):
                #     self.component_raw_output[i].eval(feed_dict={
                #         self.u_id: batch_u,
                #         self.i_id: batch_v,
                #         self.j_id: batch_j,
                #         self.u_items_ids: batch_u_items,
                #         self.review_input: batch_u_reviews,
                #         self.dropout_keep_prob: self.dropout_keep_prob_input,
                #         self.u_items_ids_mask: batch_u_items_bask,
                #         self.batch_num: self.trainBatchSize,
                #         self.positive_item_review_input: pos_item_review,
                #         self.negative_item_review_input: neg_item_review,
                #     })
                for i in range(self.item_pad_num):
                    self.review_output_print[i].eval(feed_dict={
                        self.u_id: batch_u,
                        self.i_id: batch_v,
                        self.j_id: batch_j,
                        self.u_items_ids: batch_u_items,
                        self.dropout_keep_prob: self.dropout_keep_prob_input,
                        self.u_pos_items_ids_mask: batch_u_items_bask,
                        self.batch_num: self.trainBatchSize,

                    })
                for i in range(self.item_pad_num):
                    self.review_atten_print[i].eval(feed_dict={
                        self.u_id: batch_u,
                        self.i_id: batch_v,
                        self.j_id: batch_j,
                        self.u_items_ids: batch_u_items,
                        self.dropout_keep_prob: self.dropout_keep_prob_input,
                        self.u_pos_items_ids_mask: batch_u_items_bask,
                        self.batch_num: self.trainBatchSize,

                    })
            self.atten_i_print.eval(feed_dict={
                    self.u_id: batch_u,
                    self.i_id: batch_v,
                    self.j_id: batch_j,
                    self.u_items_ids: batch_u_items,
                    self.dropout_keep_prob: self.dropout_keep_prob_input,
                    self.u_pos_items_ids_mask: batch_u_items_bask,
                    self.batch_num: self.trainBatchSize,

            })

            # self.atten_j_print.eval(feed_dict={
            #     self.u_id: batch_u,
            #     self.i_id: batch_v,
            #     self.j_id: batch_j,
            #     self.u_items_ids: batch_u_items,
            #     self.review_input: batch_u_reviews,
            #     self.dropout_keep_prob: self.dropout_keep_prob_input,
            #     self.u_pos_items_ids_mask: batch_u_items_bask,
            #     self.batch_num: self.trainBatchSize,
            #     self.positive_item_review_input: pos_item_review,
            #     self.negative_item_review_input: neg_item_review,
            # })
            print_flag = 0
        end = time.time()

        if (epochId + 1) % 50 == 0 and batchId == 0:
            self.logger.info("----------------------------------------------------------------------")
            self.logger.info("time of collect a batch of data: " + time_cost + " seconds")
            self.logger.info(
                "rating_loss: %.4f   factor_l2_loss: %.4f" % (
                    ratingLoss, factor_l2_loss))
            self.logger.info(
                "batchId: %d epoch %d/%d   batch_loss: %.4f   time of a batch: %.4f" % (
                    batchId, epochId, self.maxIter, loss, (end - start)))
            self.evaluateRanking(epochId, batchId)
        return loss

    def attentionOutputs(self, user_embedding, curr_item_embedding, curr_item_review, reviewEmbeddings, flag):
        attention_output_list = []
        item_embeddings = []
        with tf.variable_scope("item_attention_weights", reuse=True):
            user_W = tf.get_variable(name="user_W", shape=[self.numFactor, self.numFactor // 2], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())
            item_W = tf.get_variable(name="item_W", shape=[self.numFactor, self.numFactor // 2], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())
            embed_bias = tf.get_variable(name="embed_bias", dtype=tf.float32,
                                         initializer=tf.constant(0.0, shape=[self.numFactor // 2]))
            summary_W = tf.get_variable(name="summary_W", shape=[self.numFactor // 2, 1], dtype=tf.float32,
                                        initializer=tf.contrib.layers.xavier_initializer())
            summary_b = tf.get_variable(name="summary_b", dtype=tf.float32,
                                        initializer=tf.constant(0.0))

            user_atten = tf.matmul(user_embedding, user_W)
            #curr_item_atten = tf.matmul(curr_item_embedding, item_W)
            #curr_item_review_atten = tf.matmul(curr_item_review, item_W)
            attention_outputs = None

            split_list = [1] * self.item_pad_num
            itemIds = tf.split(self.u_items_ids, split_list, 1)
            for i in range(self.item_pad_num):
                hist_item_embed = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, itemIds[i]),
                                             [-1, self.numFactor])
                item_embeddings.append(hist_item_embed)
                hist_item_atten = tf.matmul(hist_item_embed, item_W)
                review_atten = tf.matmul(reviewEmbeddings[i], item_W)
                self.review_atten_print[i] = tf.Print(review_atten, [review_atten],
                                                      message="review_atten_print%d" % (i),
                                                      summarize=self.numFactor // 2)
                #atten_embed = tf.nn.relu(
                #    user_atten + curr_item_atten + curr_item_review_atten + hist_item_atten + review_atten + embed_bias)
                atten_embed = tf.nn.relu(
                    user_atten + hist_item_atten + review_atten + embed_bias)

                atten_output = tf.matmul(atten_embed, summary_W) + summary_b

                if attention_outputs == None:
                    attention_outputs = atten_output
                else:
                    attention_outputs = tf.concat([attention_outputs, atten_output], 1)

            if flag == "p":
                # self.atten_i_print = tf.Print(attention_outputs, [attention_outputs], message="attention_i", summarize=self.item_pad_num)
                attentionValues = self.softmask(mask=self.u_pos_items_ids_mask, attentions=attention_outputs)
                self.atten_i_print = tf.Print(attentionValues, [attentionValues], message="attention_i",
                                              summarize=self.item_pad_num)
            else:
                # self.atten_j_print = tf.Print(attention_outputs, [attention_outputs], message="attention_j", summarize=self.item_pad_num)
                attentionValues = self.softmask(mask=self.u_pos_items_ids_mask, attentions=attention_outputs)
                self.atten_j_print = tf.Print(attentionValues, [attentionValues], message="attention_j",
                                              summarize=self.item_pad_num)

            attention_output_list.extend(tf.split(attentionValues, split_list, 1))

            return attention_output_list, item_embeddings

    def new_attention_output(self, user_embedding, curr_item_embedding, flag):
        attention_output_list = []
        item_embeddings = []
        with tf.variable_scope("item_attention_weights", reuse=True):
            user_W = tf.get_variable(name="user_W", shape=[self.numFactor, self.numFactor // 2], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())
            item_W = tf.get_variable(name="item_W", shape=[self.numFactor, self.numFactor // 2], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())
            embed_bias = tf.get_variable(name="embed_bias", dtype=tf.float32,
                                        initializer=tf.constant(0.0, shape=[self.numFactor // 2]))
            summary_W = tf.get_variable(name="summary_W", shape=[self.numFactor // 2, 1], dtype=tf.float32,
                                        initializer=tf.contrib.layers.xavier_initializer())
            summary_b = tf.get_variable(name="summary_b", dtype=tf.float32,
                                        initializer=tf.constant(0.0))

            user_atten = tf.matmul(user_embedding, user_W)
            curr_item_atten = tf.matmul(curr_item_embedding, item_W)
            attention_outputs = None

            split_list = [1] * self.item_pad_num
            itemIds = tf.split(self.u_items_ids, split_list, 1)
            for i in range(self.item_pad_num):
                hist_item_embed = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, itemIds[i]), [-1, self.numFactor])
                item_embeddings.append(hist_item_embed)
                hist_item_atten = tf.matmul(hist_item_embed, item_W)


                atten_embed = tf.nn.relu(curr_item_atten + hist_item_atten + embed_bias)

                atten_output = tf.matmul(atten_embed, summary_W) + summary_b

                if attention_outputs == None:
                    attention_outputs = atten_output
                else:
                    attention_outputs = tf.concat([attention_outputs, atten_output], 1)

            if flag == "p":
                # self.atten_i_print = tf.Print(attention_outputs, [attention_outputs], message="attention_i", summarize=self.item_pad_num)
                attentionValues = self.softmask(mask=self.u_pos_items_ids_mask, attentions=attention_outputs)
                self.atten_i_print = tf.Print(attentionValues, [attentionValues], message="attention_i", summarize=self.item_pad_num)
            else:
                # self.atten_j_print = tf.Print(attention_outputs, [attention_outputs], message="attention_j", summarize=self.item_pad_num)
                attentionValues = self.softmask(mask=self.u_pos_items_ids_mask, attentions=attention_outputs)
                self.atten_j_print = tf.Print(attentionValues, [attentionValues], message="attention_j", summarize=self.item_pad_num)

            attention_output_list.extend(tf.split(attentionValues, split_list, 1))

            return attention_output_list, item_embeddings

    def getPredList_ByUserIdxList(self, user_idices):
        # build test batch
        end0 = time.time()
        testBatch_user = []
        batch_u_items_total = []
        batch_u_items_total_mask = []
        batch_num = len(user_idices) * 100

        for i in range(len(user_idices)):
            userIdx = user_idices[i]
            for itemIdx in self.evalItemsForEachUser[userIdx]:
                testBatch_user.append([userIdx, itemIdx, 1.0])

            batch_u_items = self.user_item_vecs_eval[userIdx]
            batch_u_items_total.extend(batch_u_items)
            batch_u_items_total_mask.extend([self.user_review_mask[userIdx]] * 100)

        batch_u = np.array(testBatch_user)[:, 0:1].astype(np.int32)
        batch_v = np.array(testBatch_user)[:, 1:2].astype(np.int32)
        batch_u_items_total = np.array(batch_u_items_total)
        batch_u_items_total_mask = np.array(batch_u_items_total_mask)

        end1 = time.time()

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
            self.u_items_ids: batch_u_items_total,
            self.u_pos_items_ids_mask: batch_u_items_total_mask,
            self.batch_num: batch_num,

        })

        end2 = time.time()

        output_lists = []
        for i in range(len(user_idices)):
            recommendList = {}
            start = i * 100
            end = start + 100
            for j in range(start, end):
                recommendList[batch_v[j][0]] = predList[j][0]
            sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
            output_lists.append(sorted_RecItemList)

        end3 = time.time()

        return output_lists, end1 - end0, end2 - end1, end3 - end2


















