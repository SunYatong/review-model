import numpy as np
import tensorflow as tf
import random
from recommender.ReviewRecommender import ReviewRecommender
from component.RNN import RNN_Compoment
import time


class ReviewBPR(ReviewRecommender):
    def __init__(self, dataModel, config):

        super(ReviewBPR, self).__init__(dataModel, config)

        self.user_reviewVectors = dataModel.user_reviewVectors

        self.name = 'ReviewBPR'

        self.rnn_unit_num = config['rnn_unit_num']
        self.rnn_layer_num = config['rnn_layer_num']
        self.rnn_cell = config['rnn_cell']
        self.rnn_lambda = config['rnn_lambda']

        # rnn parameters

        self.review_network = RNN_Compoment(
            rnn_unit_num=self.rnn_unit_num,
            rnn_layer_num=self.rnn_layer_num,
            rnn_cell=self.rnn_cell,
            output_size=self.numFactor,
            wordvec_size=self.wordVec_size,
            input_placeholder=self.review_input,
            max_review_length=self.maxReviewLength,
            word_matrix=self.word_embedding_matrix,
            review_wordId_print=self.review_wordId_print,
            review_input_print=self.review_input_print,
            rnn_lambda=self.rnn_lambda,
            dropout_keep_prob=self.dropout_keep_prob,
            component_raw_output=self.component_raw_output,
            item_pad_num=self.item_pad_num
        )















