import numpy as np
import tensorflow as tf
import random
import recommender.BasicRcommender
import time

class MultiCoFiSetRecommender(recommender.BasicRcommender.BasicRecommender):

    def __init__(self, dataModel, config):

        super(MultiCoFiSetRecommender, self).__init__(dataModel, config)

        self.name = 'CoFiSet'

        self.user_item_like = dataModel.user_item_like
        self.user_item_dislike = dataModel.user_item_dislike

        self.numFactor = config['numFactor']
        self.posi_num = config['posi_num']
        self.neg_num = config['neg_num']
        self.unknown_num = config['unknown_num']
        self.alpha = config['alpha']
        # placeholders
        self.u_id = tf.placeholder(tf.int32, [None, 1])
        self.i_id = tf.placeholder(tf.int32, [None, 1])
        self.pos_ids = tf.placeholder(tf.int32, [None, self.posi_num])
        self.neg_ids = tf.placeholder(tf.int32, [None, self.neg_num])
        self.unknown_ids = tf.placeholder(tf.int32, [None, self.unknown_num])
        self.factor_lambda = config['factor_lambda']

        # user/item embedding
        self.userEmbedding = tf.Variable(tf.random_normal([self.numUser, self.numFactor], 0, 0.1))
        self.itemEmbedding = tf.Variable(tf.random_normal([self.numItem, self.numFactor], 0, 0.1))
        # self.itemBias = tf.Variable(tf.random_normal([self.numItem, 1], 0, 0.1))

        self.sampleSize = self.trainSize

    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:
            userEmbedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])
            itemEmbedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, self.i_id), [-1, self.numFactor])

            itemEmedding_pos = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, self.pos_ids), [-1, self.posi_num, self.numFactor])
            itemEmedding_neg = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, self.neg_ids), [-1, self.neg_num, self.numFactor])
            itemEmedding_unknown = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, self.unknown_ids),
                                          [-1, self.unknown_num, self.numFactor])

            itemEmedding_pos = tf.reduce_sum(itemEmedding_pos, axis=1, keep_dims=False)
            itemEmedding_neg = tf.reduce_sum(itemEmedding_neg, axis=1, keep_dims=False)
            itemEmedding_unknown = tf.reduce_sum(itemEmedding_unknown, axis=1, keep_dims=False)

            pred_pos = tf.reduce_sum(tf.multiply(userEmbedding, itemEmedding_pos), 1, keep_dims=True) / self.posi_num #+ bias_i
            pred_neg = tf.reduce_sum(tf.multiply(userEmbedding, itemEmedding_neg), 1, keep_dims=True) / self.neg_num #+ bias_j
            pred_unknown = tf.reduce_sum(tf.multiply(userEmbedding, itemEmedding_unknown), 1,
                                     keep_dims=True) / self.unknown_num  # + bias_j

            pos_neg_predDiff = pred_pos - pred_neg
            pos_unknown_predDiff = pred_pos - pred_unknown

            l2_norm = self.factor_lambda * tf.nn.l2_loss(userEmbedding) \
                     + self.factor_lambda * tf.nn.l2_loss(itemEmedding_pos) \
                     + self.factor_lambda * tf.nn.l2_loss(itemEmedding_neg)

            self.r_pred = tf.reduce_sum(tf.multiply(userEmbedding, itemEmbedding), 1, keep_dims=True)

            bprLoss = - tf.reduce_sum(tf.log(tf.sigmoid(pos_neg_predDiff))) - self.alpha * tf.reduce_sum(tf.log(tf.sigmoid(pos_unknown_predDiff)))
            self.cost = bprLoss + l2_norm

    def trainEachBatch(self, epochId, batchId):
        totalLoss = 0
        start = time.time()
        batch_u, batch_pos, batch_neg, batch_unknown = self.getTrainData(batchId)

        self.optimizer.run(feed_dict={
            self.u_id: batch_u,
            self.pos_ids: batch_pos,
            self.neg_ids: batch_neg,
            self.unknown_ids: batch_unknown
        })
        loss = self.cost.eval(feed_dict={
            self.u_id: batch_u,
            self.pos_ids: batch_pos,
            self.neg_ids: batch_neg,
            self.unknown_ids: batch_unknown
        })
        totalLoss += loss
        end = time.time()
        if epochId % 5 == 0 and batchId == 0:
            self.logger.info("----------------------------------------------------------------------")
            self.logger.info(
            "batchId: %d epoch %d/%d   batch_loss: %.4f   time of a batch: %.4f" % (
                batchId, epochId, self.maxIter, totalLoss, (end - start)))

            self.evaluateRanking(epochId, batchId)
        return totalLoss


    def getTrainData(self, batchId):
        # compute start and end
        start = time.time()
        user_batch = []
        pos_item_batch = []
        neg_item_batch = []
        unknown_batch = []
        for i in range(self.trainBatchSize):
            pos_ids = []
            unknown_ids = []
            neg_ids = []
            userIdx = random.randint(0, self.numUser - 1)

            train_items = self.user_items_train[userIdx]
            positiveItems = self.user_item_like[userIdx]

            while len(pos_ids) < self.posi_num:
                positiveItemIdx = positiveItems[random.randint(0, len(positiveItems) - 1)]
                if positiveItemIdx not in pos_ids:
                    pos_ids.append(positiveItemIdx)

            while len(unknown_ids) < self.unknown_num:
                unknownItemIdx = random.randint(0, self.numItem - 1)
                if unknownItemIdx not in unknown_ids and unknownItemIdx not in train_items:
                    unknown_ids.append(unknownItemIdx)

            if userIdx in self.user_item_dislike:
                negativeItems = self.user_item_dislike[userIdx]

                while len(neg_ids) < self.neg_num:
                    negativeItemIdx = negativeItems[random.randint(0, len(negativeItems) - 1)]
                    if negativeItemIdx not in neg_ids:
                        neg_ids.append(negativeItemIdx)
            else:
                neg_ids = unknown_ids

            user_batch.append(userIdx)
            pos_item_batch.append(pos_ids)
            neg_item_batch.append(neg_ids)
            unknown_batch.append(unknown_ids)

        batch_u = np.array(user_batch).reshape((self.trainBatchSize, 1))
        batch_pos = np.array(pos_item_batch).reshape((self.trainBatchSize, self.posi_num))
        batch_neg = np.array(neg_item_batch).reshape((self.trainBatchSize, self.neg_num))
        batch_unknown = np.array(unknown_batch).reshape((self.trainBatchSize, self.unknown_num))

        end = time.time()
        # self.logger.info("time of collect a batch of data: " + str((end - start)) + " seconds")

        return batch_u, batch_pos, batch_neg, batch_unknown

    def getPredList_ByUserIdxList(self, user_idices):
        end0 = time.time()
        # build test batch
        testBatch_user = []

        for i in range(len(user_idices)):
            userIdx = user_idices[i]
            for itemIdx in self.evalItemsForEachUser[userIdx]:
                testBatch_user.append([userIdx, itemIdx, 1.0])

        batch_u = np.array(testBatch_user)[:, 0:1].astype(np.int32)
        batch_v = np.array(testBatch_user)[:, 1:2].astype(np.int32)
        end1 = time.time()

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
        })
        end2 = time.time()

        output_lists = []
        for i in range(len(user_idices)):
            recommendList = {}
            start = i * 100
            end = start + 100
            for j in range(start, end):
                recommendList[batch_v[j][0]] = predList[j][0]
            sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
            output_lists.append(sorted_RecItemList)
        end3 = time.time()

        return output_lists, end1 - end0, end2 - end1, end3 - end2

    def getPredList_ByUserIdx(self, userIdx):
        # build test batch
        testBatch_user = []

        for itemIdx in self.evalItemsForEachUser[userIdx]:
            testBatch_user.append([userIdx, itemIdx, 1.0])

        batch_u = np.array(testBatch_user)[:, 0:1].astype(np.int32)
        batch_v = np.array(testBatch_user)[:, 1:2].astype(np.int32)

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
        })

        recommendList = {}
        for i in range(len(testBatch_user)):
            recommendList[batch_v[i][0]] = predList[i][0]

        sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]

        return sorted_RecItemList