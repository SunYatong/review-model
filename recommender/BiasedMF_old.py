import numpy as np
import tensorflow as tf
import time
import random
import recommender.BasicRcommender
import math

class BiasedMFRecommender(recommender.BasicRcommender.BasicRecommender):

    def __init__(self, dataModel, config):
        super(BiasedMFRecommender, self).__init__(dataModel, config)

        self.min_rmse = 100
        self.min_mae = 100
        self.min_epoch = 0
        self.min_batchId = 0
        self.min_bounded_rmse = 0
        self.min_bounded_mae = 0
        self.resultList = None

        self.name = 'BiasedMF'
        self.numFactor = config['numFactor']
        self.lam1 = config['lam1']
        self.learnRate = config['learnRate']
        self.maxIter = config['maxIter']
        self.batchSize = config['trainBatchSize']
        self.trainType = config['trainType']


        tf.set_random_seed(123)
        random.seed(123)

        self.logger = dataModel.logger
        self.numUser = dataModel.numUser
        self.numItem = dataModel.numItem
        self.minRating = 1.0
        self.maxRating = 5.0
        self.trainSet = np.array(dataModel.trainSet)
        self.testSet = np.array(dataModel.testSet)
        self.trainSize = len(self.trainSet)
        self.optiType = 'gd'

        self.globalMean = np.mean(self.trainSet[:, 2:3])
        self.batch_num = int(self.trainSize // self.batchSize) + 1

        # placeholders
        self.u_input = tf.placeholder(tf.int32,   [None, 1])
        self.i_input = tf.placeholder(tf.int32,   [None, 1])
        self.r_label = tf.placeholder(tf.float32, [None, 1])

        # weights
        scale = 1 / math.sqrt(self.numUser)
        tf.set_random_seed(123)
        self.weights = {
            'u_factor': tf.Variable(tf.random_uniform([self.numUser, self.numFactor], -scale, scale),
                                    name='u_encoder_weight', dtype=tf.float32),
            'i_factor': tf.Variable(tf.random_uniform([self.numItem, self.numFactor], -scale, scale),
                                name='i_factor', dtype=tf.float32),
            'u_bias': tf.Variable(tf.random_uniform([self.numUser], -scale, scale),
                                     name='u_bias', dtype=tf.float32),
            'i_bias': tf.Variable(tf.random_uniform([self.numItem], -scale, scale),
                                  name='i_bias', dtype=tf.float32),
            'implicit_vectors': tf.Variable(tf.random_uniform([self.numItem, self.numFactor], -scale, scale),
                                  name='implicit_vectors', dtype=tf.float32),
        }

        self.r_pred = None
        self.cost = None
        self.rmse = None
        self.mae = None
        self.optimizer = None

    def buildModel(self):

        u_factor = tf.reshape(tf.nn.embedding_lookup(self.weights['u_factor'], self.u_input), [-1, self.numFactor])
        i_factor = tf.reshape(tf.nn.embedding_lookup(self.weights['i_factor'], self.i_input), [-1, self.numFactor])

        u_bias = tf.reshape(tf.nn.embedding_lookup(self.weights['u_bias'], self.u_input), [-1, 1])
        i_bias = tf.reshape(tf.nn.embedding_lookup(self.weights['i_bias'], self.i_input), [-1, 1])

        self.r_pred = tf.reduce_sum(tf.multiply(u_factor, i_factor), 1, keep_dims=True) + u_bias + i_bias + self.globalMean


        self.cost = tf.reduce_sum(tf.square(self.r_label - self.r_pred)) \
                     + self.lam1 * tf.nn.l2_loss(self.weights['u_factor']) \
                     + self.lam1 * tf.nn.l2_loss(self.weights['i_factor']) \
                     + self.lam1 * tf.nn.l2_loss(self.weights['u_bias']) \
                     + self.lam1 * tf.nn.l2_loss(self.weights['i_bias']) \

        self.cost = self.cost * 0.5

        self.rmse = tf.sqrt(tf.reduce_mean(tf.square(self.r_label - self.r_pred)))
        self.mae = tf.reduce_mean(tf.abs(self.r_label - self.r_pred))

    def trainModel(self):
        self.printConfig()

        sess = tf.InteractiveSession()
        self.optimizer = self.getOptimizer()
        sess.run(tf.global_variables_initializer())


        test_u, test_v, test_r = self.getTestData()


        start_time = time.time()
        isNaN = False
        overFitCount = 0
        isOverFitting = False
        for epoch in range(self.maxIter):
            for i in range(self.batch_num):

                batch_u, batch_v, batch_r = self.getTrainData(i)
                self.optimizer.run(feed_dict={
                    self.u_input: batch_u,
                    self.i_input: batch_v,
                    self.r_label: batch_r,
                })
                loss = self.cost.eval(feed_dict={
                    self.u_input: batch_u,
                    self.i_input: batch_v,
                    self.r_label: batch_r,
                })
                rmseResult = self.rmse.eval(feed_dict={
                    self.u_input: test_u,
                    self.i_input: test_v,
                    self.r_label: test_r,
                })
                maeResult = self.mae.eval(feed_dict={
                    self.u_input: test_u,
                    self.i_input: test_v,
                    self.r_label: test_r,
                })
                predList = sess.run(self.r_pred, feed_dict={
                    self.u_input: test_u,
                    self.i_input: test_v,
                })
                # print(predList)
                self.ratingBound(predList)
                bounded_rmse_result = self.rmse.eval(feed_dict={self.r_pred: predList,
                                                                self.r_label: test_r})
                bounded_mae_result = self.mae.eval(feed_dict={self.r_pred: predList,
                                                              self.r_label: test_r})

                self.saveMinRMSE(epoch, i, rmseResult, bounded_rmse_result, maeResult, bounded_mae_result, predList)
                self.logger.info(
                    "batchId: %d epoch %d/%d   loss: %.4f  rmse: %.4f   bounded_rmse: %.4f  mae: %.4f   bounded_mae: %.4f" % (
                        i, epoch + 1, self.maxIter, loss, rmseResult, bounded_rmse_result, maeResult, bounded_mae_result))

                if math.isnan(rmseResult) or (rmseResult > 2000 and epoch > 50):
                    isNaN = True
                    break
                if rmseResult > self.min_rmse:
                    overFitCount += 1
                    self.logger.info('overFitCount: ' + str(overFitCount))
                else:
                    overFitCount = 0
                if overFitCount > 300:
                    isOverFitting = True
                    break

            if isNaN or isOverFitting:
                break

        end_time = time.time()
        trainTime = end_time - start_time
        self.printMinRMSE(isNaN, trainTime)
        # self.printResult(test_u, test_v, test_r)
        tf.reset_default_graph()

    def getTrainData(self, batchId):
        # compute start and end
        start = batchId * self.batchSize
        end = start + self.batchSize
        if end > self.trainSize:
            end = self.trainSize

        batch_u = self.trainSet[start:end, 0:1].astype(np.int64)
        batch_v = self.trainSet[start:end, 1:2].astype(np.int64)
        batch_r = self.trainSet[start:end, 2:3]

        return np.array(batch_u), np.array(batch_v), np.array(batch_r)

    def getTestData(self):
        test_u = self.testSet[:, 0:1].astype(np.int64)
        test_v = self.testSet[:, 1:2].astype(np.int64)
        test_r = self.testSet[:, 2:3]

        return np.array(test_u), np.array(test_v), np.array(test_r)

    def ratingBound(self, ratingList):
        for i in range(len(ratingList)):
            if ratingList[i][0] > self.maxRating:
                ratingList[i][0] = self.maxRating
            if ratingList[i][0] < self.minRating:
                ratingList[i][0] = self.minRating

    def saveMinRMSE(self, epoch, batchId, rmse, bounded_rmse, mae, bounded_mae, predList):
        if rmse < self.min_rmse:
            self.min_epoch = epoch
            self.min_batchId = batchId

            self.min_rmse = rmse
            self.min_bounded_rmse = bounded_rmse

            self.min_mae = mae
            self.min_bounded_mae = bounded_mae

            self.resultList = predList

    def printMinRMSE(self, isNaN, trainTime):
        if isNaN:
            self.logger.info("NaN error!")

        self.logger.info(str(self.name) + " result infomation")
        self.logger.info('trainType: ' + self.trainType)
        self.logger.info('trainTime: ' + str(trainTime) + 'seconds')

        self.logger.info('trainSize: ' + str(self.trainSize))

        self.logger.info(
            "converge at epoch %d  batchId %d  min_rmse: %.4f  min_bounded_rmse: %.4f  min_mae: %.4f  min_bounded_mae: %.4f" % (
                self.min_epoch, self.min_batchId, self.min_rmse, self.min_bounded_rmse, self.min_mae, self.min_bounded_mae))

        self.printConfig()
        self.logger.info(' ############################################# ')

    def printConfig(self):
        self.logger.info('Recommender: ' + str(self.name))
        self.logger.info('learnRate: ' + str(self.learnRate))
        self.logger.info('globalMean: ' + str(self.globalMean))
        self.logger.info('numFactor: ' + str(self.numFactor))

    def getOptimizer(self):
        if self.optiType == 'gd':
            return tf.train.GradientDescentOptimizer(self.learnRate, name='GD_optimizer').minimize(self.cost)
        elif self.optiType == 'adam':
            return tf.train.AdamOptimizer(self.learnRate, name='Adam_optimizer').minimize(self.cost)
        elif self.optiType == 'adadelta':
            return tf.train.AdadeltaOptimizer(self.learnRate, name='Adadelta_optimizer').minimize(self.cost)
        elif self.optiType == 'rmsprop':
            return tf.train.RMSPropOptimizer(self.learnRate, name='RMSProp_optimizer').minimize(self.cost)
        else:
            return None

    def getActiv(self, input, typeString):
        if typeString == 'tanh':
            return tf.nn.tanh(input)
        elif typeString == 'relu':
            return tf.nn.relu(input)
        elif typeString == 'sigmoid':
            return tf.nn.sigmoid(input)
        elif typeString == 'identical':
            return input
        else:
            return None





