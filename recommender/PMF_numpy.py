import numpy as np
import tensorflow as tf
import random
import recommender.BasicRcommender
import time

class PMFRecommender(recommender.BasicRcommender.BasicRecommender):

    def __init__(self, dataModel, config):

        super(PMFRecommender, self).__init__(dataModel, config)

        self.trainSet = dataModel.trainSet
        self.testSet = dataModel.testSet

        self.name = 'PMF_numpy'
        self.negative = config['negative_sample']

        self.numFactor = config['numFactor']
        self.lam1 = config['lam1']

        # user/item embedding
        tf.set_random_seed(123)
        random.seed(123)
        self.userEmbedding = np.random.normal(loc=0, scale=0.1, size=(self.numUser, self.numFactor))
        self.itemEmbedding = np.random.normal(loc=0, scale=0.1, size=(self.numItem, self.numFactor))


    def trainEachBatch(self, epochId, batchId):
        loss = 0
        for userIdx, itemIdx, r_label in self.trainSet:
            userVec = self.userEmbedding[userIdx]
            itemVec = self.itemEmbedding[itemIdx]
            r_pred = self.sigmoid(np.dot(userVec, itemVec))

            error = r_label - r_pred
            loss += error * error

            for k in range(self.numFactor):
                loss += self.lam1 * userVec[k] * userVec[k] + self.lam1 * itemVec[k] * itemVec[k]
                d_cost_d_uk = -error * itemVec[k] + self.lam1 * userVec[k]
                d_cost_d_vk = -error * userVec[k] + self.lam1 * itemVec[k]
                userVec[k] -= self.learnRate * d_cost_d_uk
                itemVec[k] -= self.learnRate * d_cost_d_vk
        loss *= 0.5

        self.logger.info("----------------------------------------------------------------------")
        self.logger.info(
            "batchId: %d epoch %d/%d   loss: %.4f" % (batchId, epochId, self.maxIter, loss))
        if self.goal == 'ranking':
            self.evaluateRanking(epochId, batchId)
        else:
            self.evaluateRating(epochId, batchId)

    def trainModel(self):
        self.sess = tf.InteractiveSession()
        self.sess.run(tf.global_variables_initializer())
        for epochId in range(self.maxIter):
            start = time.time()
            for batchId in range(self.trainBatchNum):
                self.trainEachBatch(epochId, batchId)
            end = time.time()
            self.logger.info("time cost of an epoch:" + str(end - start))


    def getRatingPredictions(self):
        predList = []
        for userIdx, itemIdx, _ in self.testSet:
            r_pred = np.dot(self.userEmbedding[userIdx], self.itemEmbedding[itemIdx])
            predList.append(r_pred)
        predList = np.array(predList)
        predList = predList.reshape((self.testSize, 1))

        for i in range(len(predList)):
            if predList[i][0] > 5:
                predList[i][0] = 5
            if predList[i][0] < 1:
                predList[i][0] = 1

        # print(predList)
        return predList


    def getPredList_ByUserIdx(self, userIdx):
        # build test batch
        predList = []
        itemList = []

        for itemIdx in self.evalItemsForEachUser[userIdx]:
            itemList.append(itemIdx)
            r_pred = self.sigmoid(np.dot(self.userEmbedding[userIdx], self.itemEmbedding[itemIdx]))
            predList.append(r_pred)

        recommendList = {}
        for i in range(len(predList)):
            itemIdx = itemList[i]
            r_pred = predList[i]
            recommendList[itemIdx] = r_pred

        sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]

        return sorted_RecItemList












