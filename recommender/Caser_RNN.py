import numpy as np
import tensorflow as tf
import random
from recommender.BasicRcommender import BasicRecommender
import time
from component.Conv_Pool import CNN_Pool_Compoment
from component.RNN import RNN_Compoment
from component.MLP import MLP

class Caser_RNN_Recommender(BasicRecommender):

    def __init__(self, dataModel, config):

        super(Caser_RNN_Recommender, self).__init__(dataModel, config)

        self.name = 'Caser_RNN'

        self.numFactor = config['numFactor']
        self.factor_lambda = config['factor_lambda']
        self.input_length = config['input_length']
        self.target_length = config['target_length']
        self.hor_filter_num = config['hor_filter_num']
        self.ver_filter_num = config['ver_filter_num']
        self.dropout_keep = config['dropout_keep']

        self.rnn_unit_num = config['rnn_unit_num']
        self.rnn_layer_num = config['rnn_layer_num']
        self.rnn_cell = config['rnn_cell']
        self.if_local_position = config['if_local_position']
        self.if_global_position = config['if_global_position']

        self.train_users = dataModel.train_users
        self.train_seq_cnn = dataModel.train_sequences_input
        self.train_seq_rnn = dataModel.train_sequences_rnn
        self.train_seq_target = dataModel.train_sequences_target

        self.pred_seq_cnn = dataModel.user_pred_sequences
        self.pred_seq_rnn = dataModel.user_pred_sequences_rnn

        self.train_pos = dataModel.train_pos
        self.test_pos = dataModel.test_pos
        self.max_pos = dataModel.max_pos

        self.trainSize = len(self.train_users)
        self.trainBatchNum = int(self.trainSize // self.trainBatchSize) + 1

        self.item_fc_dim = [self.hor_filter_num * self.input_length + self.ver_filter_num * self.numFactor] + config['item_fc_dim']

        # placeholders
        self.u_id = tf.placeholder(tf.int32, [None, 1])
        self.cnn_input_seq = tf.placeholder(tf.int32, [None, self.input_length])
        self.rnn_input_seq_train = tf.placeholder(tf.int32, [None, self.target_length])
        self.rnn_input_seq_test = tf.placeholder(tf.int32, [None, 1])

        self.target_seq_pos = tf.placeholder(tf.int32, [None, self.target_length])
        self.target_seq_neg = tf.placeholder(tf.int32, [None, self.target_length])
        self.pred_seq = tf.placeholder(tf.int32, [None, 100])

        self.global_pos_input = tf.placeholder(tf.int32, [None, 1])

        self.dropout_keep_placeholder = tf.placeholder_with_default(1.0, shape=())

        # user/item embedding
        self.userEmbedding = tf.Variable(tf.random_normal([self.numUser, self.numFactor], 0, 0.1))
        self.itemEmbedding = tf.Variable(tf.random_normal([self.numItem, self.numFactor], 0, 0.1))

        self.local_position_embed = None
        self.global_position_embed = None
        if self.if_local_position:
            self.local_position_embed = tf.Variable(tf.random_normal([self.input_length, self.numFactor], 0, 0.1))
        if self.if_global_position:
            self.global_position_embed = tf.Variable(tf.random_normal([self.max_pos, self.numFactor], 0, 0.1))

        self.vertical_CNN = CNN_Pool_Compoment(
            filter_num=self.ver_filter_num,
            filter_sizes=[self.input_length],
            wordvec_size=self.numFactor,
            max_review_length=self.input_length,
            word_matrix=self.itemEmbedding,
            output_size=1,
            review_wordId_print=None,
            review_input_print=None,
            cnn_lambda=None,
            dropout_keep_prob=None,
            component_raw_output=None,
            item_pad_num=None,
            name='ver'
        )

        self.horizontal_CNN = CNN_Pool_Compoment(
            filter_num=self.hor_filter_num,
            filter_sizes=[i+1 for i in range(self.input_length)],
            wordvec_size=self.numFactor,
            max_review_length=self.input_length,
            word_matrix=self.itemEmbedding,
            output_size=1,
            review_wordId_print=None,
            review_input_print=None,
            cnn_lambda=None,
            dropout_keep_prob=None,
            component_raw_output=None,
            item_pad_num=None,
            name='hor'
        )

        self.rnn_network = RNN_Compoment(
            rnn_unit_num=self.rnn_unit_num,
            rnn_layer_num=self.rnn_layer_num,
            rnn_cell=self.rnn_cell,
            output_size=self.numFactor,
            wordvec_size=self.numFactor,
            input_placeholder=None,
            max_review_length=None,
            word_matrix=self.itemEmbedding,
            review_wordId_print=None,
            review_input_print=None,
            rnn_lambda=None,
            dropout_keep_prob=self.dropout_keep_placeholder,
            component_raw_output=None,
            item_pad_num=None
        ).return_network()

        self.item_MLP = MLP(self.item_fc_dim, dropout_keep=self.dropout_keep_placeholder)

        self.fea_h_W = tf.get_variable(
            name="fea_h_W",
            dtype=tf.float32,
            shape=[self.item_fc_dim[-1] + self.numFactor, self.rnn_unit_num],
            initializer=tf.contrib.layers.xavier_initializer()
        )

        self.fea_h_b = tf.get_variable(
            name="fea_h_b",
            dtype=tf.float32,
            initializer=tf.constant(0.1, shape=[self.rnn_unit_num])
        )

        self.output_fc_W = tf.get_variable(
            name="output_fc_W",
            dtype=tf.float32,
            shape=[self.numItem, self.rnn_unit_num],
            initializer=tf.contrib.layers.xavier_initializer()
        )

        self.output_fc_b = tf.get_variable(
            name="output_fc_b",
            dtype=tf.float32,
            initializer=tf.constant(0.1, shape=[self.numItem, 1])
        )


    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:

            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])
            global_pos_embed = None
            if self.if_global_position:
                global_pos_embed = tf.reshape(tf.nn.embedding_lookup(self.global_position_embed, self.global_pos_input), [-1, self.numFactor])
                global_pos_embed = tf.expand_dims(global_pos_embed, axis=1)
            ver_embed = self.vertical_CNN.get_vertical_output(self.cnn_input_seq,
                                                              local_position_embed=self.local_position_embed,
                                                              globla_position_embed=global_pos_embed)

            hor_embed = self.horizontal_CNN.get_horizontal_output(self.cnn_input_seq,
                                                              local_position_embed=self.local_position_embed,
                                                              globla_position_embed=global_pos_embed)

            item_fc_feature = tf.concat([hor_embed, ver_embed], axis=1)

            item_fc_output = self.item_MLP.get_output(feature_input=item_fc_feature)

            user_fc_feature = tf.concat([item_fc_output, userEmedding], axis=1)

            init_state = tf.nn.xw_plus_b(x=user_fc_feature, weights=self.fea_h_W, biases=self.fea_h_b)

            rnn_train_item_embed_input = tf.nn.embedding_lookup(self.itemEmbedding, self.rnn_input_seq_train)
            # Batch size x time steps x features.
            rnn_train_item_embed_input = tf.reshape(rnn_train_item_embed_input, [-1, self.target_length, self.numFactor])

            rnn_train_outputs, curr_train_state = tf.nn.dynamic_rnn(
                cell=self.rnn_network,
                inputs=rnn_train_item_embed_input,
                dtype=tf.float32,
                initial_state=tuple([init_state for i in range(self.rnn_layer_num)])
            )

            pos_pred = self.pred_for_a_user(
                W=self.output_fc_W,
                b=self.output_fc_b,
                numFactor=self.rnn_unit_num,
                input_feature=curr_train_state[-1],
                ids=self.target_seq_pos,
                tar_length=self.target_length
            )

            neg_pred = self.pred_for_a_user(
                W=self.output_fc_W,
                b=self.output_fc_b,
                numFactor=self.rnn_unit_num,
                input_feature=curr_train_state[-1],
                ids=self.target_seq_neg,
                tar_length=self.target_length
            )

            bpr_loss = - tf.reduce_sum(tf.log(tf.sigmoid(pos_pred - neg_pred)))

            # pos_loss = - tf.reduce_mean(tf.log(tf.nn.sigmoid(pos_pred)))
            # neg_loss = - tf.reduce_mean(tf.log(1 - tf.nn.sigmoid(neg_pred)))

            self.cost = bpr_loss

            rnn_test_item_embed_input = tf.nn.embedding_lookup(self.itemEmbedding, self.rnn_input_seq_test)
            # Batch size x time steps x features.
            rnn_test_item_embed_input = tf.reshape(rnn_test_item_embed_input, [-1, 1, self.numFactor])

            rnn_test_outputs, curr_test_state = tf.nn.dynamic_rnn(
                cell=self.rnn_network,
                inputs=rnn_test_item_embed_input,
                dtype=tf.float32,
                initial_state=tuple([init_state for i in range(self.rnn_layer_num)])
            )

            self.r_pred = self.pred_for_a_user(
                W=self.output_fc_W,
                b=self.output_fc_b,
                numFactor=self.rnn_unit_num,
                input_feature=curr_test_state[-1],
                ids=self.pred_seq,
                tar_length=100
            )

    def trainEachBatch(self, epochId, batchId):
        totalLoss = 0
        start = time.time()
        feed_dict = self.getTrainData(batchId)

        # print(batchId)
        self.optimizer.run(feed_dict=feed_dict)
        loss = self.cost.eval(feed_dict=feed_dict)
        totalLoss += loss
        end = time.time()
        if epochId % 5 == 0 and batchId == 0:
            self.logger.info("----------------------------------------------------------------------")
            self.logger.info(
                "batchId: %d epoch %d/%d   batch_loss: %.4f   time of a batch: %.4f" % (
                    batchId, epochId, self.maxIter, totalLoss, (end - start)))

            self.evaluateRanking(epochId, batchId)
        return totalLoss

    def getTrainData(self, batchId):
        # compute start and end
        start = time.time()

        user_batch = []
        cnn_seq_batch = []
        rnn_seq_batch = []
        pos_tar_batch = []
        neg_tar_batch = []
        global_pos = []

        start_idx = batchId * self.trainBatchSize
        end_idx = start_idx + self.trainBatchSize

        if end_idx > self.trainSize:
            end_idx = self.trainSize

        if end_idx == start_idx:
            start_idx = 0
            end_idx = start_idx + self.trainBatchSize

        if end_idx > self.trainSize:
            end_idx = self.trainSize

        user_batch = self.train_users[start_idx:end_idx]
        global_pos = self.train_pos[start_idx:end_idx]
        cnn_seq_batch = self.train_seq_cnn[start_idx:end_idx]
        rnn_seq_batch = self.train_seq_rnn[start_idx:end_idx]
        pos_seq_batch = self.train_seq_target[start_idx:end_idx]

        for userIdx in user_batch:
            neg_items = []
            for i in range(self.target_length):
                positiveItems = self.user_items_train[userIdx]
                negativeItemIdx = random.randint(0, self.numItem - 1)
                while negativeItemIdx in positiveItems:
                    negativeItemIdx = random.randint(0, self.numItem - 1)
                neg_items.append(negativeItemIdx)
            neg_tar_batch.append(neg_items)

        user_batch = np.array(user_batch).reshape((end_idx - start_idx, 1))
        cnn_seq_batch = np.array(cnn_seq_batch)
        rnn_seq_batch = np.array(rnn_seq_batch)
        pos_seq_batch = np.array(pos_seq_batch)
        neg_seq_batch = np.array(neg_tar_batch)
        global_pos = np.array(global_pos).reshape((end_idx - start_idx, 1))

        end = time.time()

        feed_dict = {
            self.u_id: user_batch,
            self.cnn_input_seq: cnn_seq_batch,
            self.rnn_input_seq_train: rnn_seq_batch,
            self.target_seq_pos: pos_seq_batch,
            self.target_seq_neg: neg_seq_batch,
            self.dropout_keep_placeholder: self.dropout_keep,
            self.global_pos_input: global_pos
        }

        return feed_dict

    # def getPredList_ByUserIdxList(self, user_idices):
    #     end0 = time.time()
    #     # build test batch
    #     testBatch_user = []
    #
    #     for i in range(len(user_idices)):
    #         userIdx = user_idices[i]
    #         pred_seq = self.user_pred_sequences[userIdx]
    #         for itemIdx in self.evalItemsForEachUser[userIdx]:
    #             testBatch_user.append([userIdx] + pred_seq + [itemIdx])
    #
    #     testBatch_user = np.array(testBatch_user)
    #
    #     batch_u = testBatch_user[:, 0:1].astype(np.int32)
    #     input_seq = testBatch_user[:, 1:6].astype(np.int32)
    #     target_seq = testBatch_user[:, 6:].astype(np.int32)
    #     end1 = time.time()
    #
    #     predList = self.sess.run(self.r_pred, feed_dict={
    #             self.u_id: batch_u,
    #             self.input_seq: input_seq,
    #             self.pred_seq: target_seq,
    #     })
    #     end2 = time.time()
    #
    #     output_lists = []
    #     for i in range(len(user_idices)):
    #         recommendList = {}
    #         start = i * 100
    #         end = start + 100
    #         for j in range(start, end):
    #             recommendList[target_seq[j][0]] = predList[j][0]
    #         sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
    #         output_lists.append(sorted_RecItemList)
    #     end3 = time.time()
    #
    #     return output_lists, end1 - end0, end2 - end1, end3 - end2

    def getPredList_ByUserIdxList(self, user_idices):
        end0 = time.time()
        # build test batch
        cnn_input_seq = []
        rnn_input_seq = []
        target_seq = []
        global_pos = []

        for userIdx in user_idices:
            cnn_input_seq.append(self.pred_seq_cnn[userIdx])
            rnn_input_seq.append(self.pred_seq_rnn[userIdx])
            target_seq.append(self.evalItemsForEachUser[userIdx])
            global_pos.append(self.test_pos[userIdx])

        batch_u = np.array(user_idices).reshape((-1, 1))
        cnn_input_seq = np.array(cnn_input_seq)
        target_seq = np.array(target_seq)
        global_pos = np.array(global_pos).reshape((-1, 1))

        end1 = time.time()

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_id: batch_u,
            self.cnn_input_seq: cnn_input_seq,
            self.rnn_input_seq_test: rnn_input_seq,
            self.pred_seq: target_seq,
            self.global_pos_input: global_pos,
        })
        end2 = time.time()

        output_lists = []
        for i in range(len(user_idices)):
            recommendList = {}
            start = i * 100
            end = start + 100
            for j in range(end-start):
                recommendList[target_seq[i][j]] = predList[i][j]
            sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
            output_lists.append(sorted_RecItemList)
        end3 = time.time()

        return output_lists, end1 - end0, end2 - end1, end3 - end2

        # def getPredList_ByUserIdx(self, userIdx):
        #     # build test batch
        #     testBatch_user = []
        #
        #     for itemIdx in self.evalItemsForEachUser[userIdx]:
        #         testBatch_user.append([userIdx, itemIdx, 1.0])
        #
        #     batch_u = np.array(testBatch_user)[:, 0:1].astype(np.int32)
        #     batch_v = np.array(testBatch_user)[:, 1:2].astype(np.int32)
        #
        #     predList = self.sess.run(self.r_pred, feed_dict={
        #         self.u_id: batch_u,
        #         self.i_id: batch_v,
        #     })
        #
        #     recommendList = {}
        #     for i in range(len(testBatch_user)):
        #         recommendList[batch_v[i][0]] = predList[i][0]
        #
        #     sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
        #
        #     return sorted_RecItemList