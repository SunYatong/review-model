from recommender.BasicRcommender import BasicRecommender
import numpy as np
import tensorflow as tf
import random
import time

class AutoEncoder(BasicRecommender):

    def __init__(self, dataModel, config):
        super(AutoEncoder, self).__init__(dataModel, config)
        self.name = 'AutoEncoder'
        self.numFactor = config['numFactor']
        self.activName = config['activName']
        self.lossType = config['lossType']
        self.lam1 = config['lam1']
        self.item_pad_num = dataModel.item_pad_num

        self.pad_mask = dataModel.pad_mask
        self.negative_ratio = config['test_ratio']
        self.item_test_num = self.item_pad_num + 10
        self.user_negative_num = self.calculate_user_negative_num(ratio=self.negative_ratio)

        self.batch_num = tf.placeholder(tf.int32, shape=())
        self.i_ids = tf.placeholder(tf.int32, [None, self.item_pad_num])
        self.i_ids_mask = tf.placeholder(tf.float32, [None, self.item_pad_num])
        self.test_item_ids = tf.placeholder(tf.int32, [None, self.item_test_num])
        self.test_rating_label = tf.placeholder(tf.float32, [None, self.item_test_num])
        self.test_rating_mask = tf.placeholder(tf.float32, [None, self.item_test_num])

        self.pred_i_id = tf.placeholder(tf.int32, [None, 1])

        self.is_train = tf.placeholder_with_default(True, shape=())
        self.test_pred = None

        with tf.variable_scope("rating_autoencoder", reuse=None):
            encoder_W_1 = tf.get_variable(name="encoder_W_1", shape=[self.numItem, self.numFactor],
                                          dtype=tf.float32,
                                          initializer=tf.contrib.layers.xavier_initializer())
            encoder_b_1 = tf.get_variable(name="encoder_b_1", dtype=tf.float32,
                                        initializer=tf.constant(0.0, shape=[self.numFactor]))
            encoder_W_2 = tf.get_variable(name="encoder_W_2", shape=[self.numFactor, self.numFactor // 2], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())
            encoder_b_2 = tf.get_variable(name="encoder_b_2", dtype=tf.float32,
                                        initializer=tf.constant(0.0, shape=[self.numFactor // 2]))
            decoder_W_1 = tf.get_variable(name="decoder_W_1", shape=[self.numFactor // 2, self.numFactor], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())
            decoder_b_1 = tf.get_variable(name="decoder_b_1", dtype=tf.float32,
                                        initializer=tf.constant(0.0, shape=[self.numFactor]))
            decoder_W_2 = tf.get_variable(name="decoder_W_2", shape=[self.numFactor, self.numItem], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())
            decoder_b_2 = tf.get_variable(name="decoder_b_2", dtype=tf.float32,
                                        initializer=tf.constant(0.0, shape=[self.numItem]))


    def buildModel(self):
        with tf.variable_scope("rating_autoencoder", reuse=True):
            encoder_W_1 = tf.get_variable(name="encoder_W_1", shape=[self.numItem, self.numFactor],
                                          dtype=tf.float32,
                                          initializer=tf.contrib.layers.xavier_initializer())
            encoder_b_1 = tf.get_variable(name="encoder_b_1", dtype=tf.float32,
                                          initializer=tf.constant(0.0, shape=[self.numFactor]))
            encoder_W_2 = tf.get_variable(name="encoder_W_2", shape=[self.numFactor, self.numFactor // 2],
                                          dtype=tf.float32,
                                          initializer=tf.contrib.layers.xavier_initializer())
            encoder_b_2 = tf.get_variable(name="encoder_b_2", dtype=tf.float32,
                                          initializer=tf.constant(0.0, shape=[self.numFactor // 2]))
            decoder_W_1 = tf.get_variable(name="decoder_W_1", shape=[self.numFactor // 2, self.numFactor],
                                          dtype=tf.float32,
                                          initializer=tf.contrib.layers.xavier_initializer())
            decoder_b_1 = tf.get_variable(name="decoder_b_1", dtype=tf.float32,
                                          initializer=tf.constant(0.0, shape=[self.numFactor]))
            decoder_W_2 = tf.get_variable(name="decoder_W_2", shape=[self.numFactor, self.numItem], dtype=tf.float32,
                                          initializer=tf.contrib.layers.xavier_initializer())
            decoder_b_2 = tf.get_variable(name="decoder_b_2", dtype=tf.float32,
                                          initializer=tf.constant(0.0, shape=[self.numItem]))

            reg_l2_loss = self.lam1 * (tf.nn.l2_loss(encoder_W_1) + tf.nn.l2_loss(encoder_W_2) + tf.nn.l2_loss(decoder_W_1) + tf.nn.l2_loss(decoder_W_2))

            split_list = [1] * self.item_pad_num
            itemIds = tf.split(self.i_ids, split_list, 1)
            masks = tf.split(self.i_ids_mask, split_list, 1)

            itemEmbeddings = None
            for i in range(self.item_pad_num):
                itemId = itemIds[i]
                mask = masks[i]
                itemEmbedding = tf.reshape(tf.nn.embedding_lookup(encoder_W_1, itemId),
                                           [-1, self.numFactor])
                itemEmbedding = itemEmbedding * mask
                if itemEmbeddings == None:
                    itemEmbeddings = itemEmbedding
                else:
                    itemEmbeddings += itemEmbedding

            encode_1 = self.activ(self.activName, itemEmbeddings + encoder_b_1)
            encode_2 = self.activ(self.activName, tf.nn.xw_plus_b(encode_1, weights=encoder_W_2, biases=encoder_b_2))
            decode_1 = self.activ(self.activName, tf.nn.xw_plus_b(encode_2, weights=decoder_W_1, biases=decoder_b_1))

            item_output_matrix = tf.transpose(decoder_W_2)
            item_output_bias = tf.transpose(decoder_b_2)

            test_split_list = [1] * self.item_test_num
            test_item_ids = tf.split(self.test_item_ids, test_split_list, 1)
            preds = []
            for i in range(self.item_test_num):
                itemId = test_item_ids[i]
                itemEmbedding = tf.reshape(tf.nn.embedding_lookup(item_output_matrix, itemId),
                                           [-1, self.numFactor])
                itemBias = tf.reshape(tf.nn.embedding_lookup(item_output_bias, itemId), [-1, 1])
                pred = tf.reduce_sum(tf.multiply(decode_1, itemEmbedding), 1, keep_dims=True) + itemBias
                preds.append(pred)

            self.pred = tf.concat(values=preds, axis=1)

            if self.lossType == 'entropy':
                pass
            else:
                self.pred = self.activ(self.activName, self.pred)

            rating_loss = self.cal_rating_loss(output=self.pred, label=self.test_rating_label, mask=self.test_rating_mask)
            self.cost = rating_loss + reg_l2_loss

            test_item_embed = tf.reshape(tf.nn.embedding_lookup(item_output_matrix, self.pred_i_id), [-1, self.numFactor])
            test_item_bias = tf.reshape(tf.nn.embedding_lookup(item_output_bias, self.pred_i_id), [-1, 1])
            self.test_pred = tf.reduce_sum(tf.multiply(decode_1, test_item_embed), 1, keep_dims=True) + test_item_bias



    def trainEachBatch(self, epochId, batchId):
        totalLoss = 0
        start = time.time()

        train_ids, train_mask, test_item_ids, test_rating_label, test_rating_mask = self.getTrainData(batchId)

        self.optimizer.run(feed_dict={
            self.i_ids: train_ids,
            self.i_ids_mask: train_mask,
            self.test_item_ids: test_item_ids,
            self.test_rating_label: test_rating_label,
            self.test_rating_mask: test_rating_mask

        })
        loss = self.cost.eval(feed_dict={
            self.i_ids: train_ids,
            self.i_ids_mask: train_mask,
            self.test_item_ids: test_item_ids,
            self.test_rating_label: test_rating_label,
            self.test_rating_mask: test_rating_mask
        })
        totalLoss += loss
        end = time.time()
        if epochId % 5 == 0 and batchId == 0:
            self.logger.info("----------------------------------------------------------------------")
            self.logger.info(
            "batchId: %d epoch %d/%d   batch_loss: %.4f   time of a batch: %.4f" % (
                batchId, epochId, self.maxIter, totalLoss, (end - start)))

            self.evaluateRanking(epochId, batchId)
        return totalLoss

    def calculate_user_negative_num(self, ratio):
        user_negative_num = {}
        for userIdx, itemList in self.user_items_train.items():
            negative_num = int(len(itemList) * ratio)
            user_negative_num[userIdx] = negative_num
        return user_negative_num


    def getTrainData(self, batchId):
        # compute start and end
        start = time.time()
        train_ids = []
        train_mask = []
        test_item_ids = []
        test_rating_label = []
        test_rating_mask = []

        for userIdx, train_itemList in self.user_items_train_paded.items():
            train_itemMask = self.pad_mask[userIdx]

            train_ids.append(train_itemList)
            train_mask.append(train_itemMask)

            positiveItems = self.user_items_train[userIdx]
            negative_ids = []
            negative_num = self.user_negative_num[userIdx]

            while len(negative_ids) < 10:
                negativeItemIdx = random.randint(0, self.numItem - 1)
                while negativeItemIdx in positiveItems:
                    negativeItemIdx = random.randint(0, self.numItem - 1)
                    negative_ids.append(negativeItemIdx)

            test_ids = train_itemList + negative_ids
            rating_label = [1] * len(train_itemList) + [0] * len(negative_ids)
            rating_mask = train_mask + [1] * len(negative_ids)

            print("after user: " + str(userIdx) + ", length: " + str(len(test_ids)))

            test_item_ids.append(test_ids)
            test_rating_label.append(rating_label)
            test_rating_mask.append(rating_mask)

        train_ids = np.array(train_ids)
        train_mask = np.array(train_mask)
        test_item_ids = np.array(test_item_ids)
        test_rating_label = np.array(test_rating_label)
        test_rating_mask = np.array(test_rating_mask)

        end = time.time()
        self.logger.info("time of collect a batch of data: " + str((end - start)) + " seconds")

        return train_ids, train_mask, test_item_ids, test_rating_label, test_rating_mask


    def getPredList_ByUserIdxList(self, user_idices):
        end0 = time.time()
        # build test batch
        input_itemIds = []
        masks = []
        test_items = []

        for i in range(len(user_idices)):
            userIdx = user_idices[i]
            itemList = self.user_items_train_paded[userIdx]
            maskList = self.pad_mask[userIdx]
            for itemIdx in self.evalItemsForEachUser[userIdx]:
                input_itemIds.append(itemList)
                masks.append(maskList)
                test_items.append([itemIdx])

        input_itemIds = np.array(input_itemIds)
        masks = np.array(masks)
        test_items = np.array(test_items)

        end1 = time.time()

        predList = self.sess.run(self.test_pred, feed_dict={
            self.i_ids: input_itemIds,
            self.i_ids_mask: masks,
            self.pred_i_id: test_items,
        })
        end2 = time.time()

        output_lists = []
        for i in range(len(user_idices)):
            recommendList = {}
            start = i * 100
            end = start + 100
            for j in range(start, end):
                recommendList[test_items[j][0]] = predList[j][0]
            sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
            output_lists.append(sorted_RecItemList)
        end3 = time.time()

        return output_lists, end1 - end0, end2 - end1, end3 - end2

    # def pred_output(self, input, weight, bias):
    #     return self.activ(self.activName, tf.nn.xw_plus_b(input, weights=weight, biases=bias))
    #
    # def train_output(self, input, test_item_ids, weights, bias):
    #     split_list = [1] * self.item_test_num
    #     itemIds = tf.split(test_item_ids, split_list, 1)
    #
    #     for i in range(self.item_test_num):
    #         itemId = itemIds[i]
    #         itemEmbedding = tf.reshape(tf.nn.embedding_lookup(encoder_W_1, itemId),
    #                                    [-1, self.numFactor])
    #         pred_itemId = input * itemEmbedding + bias[itemId]
    #         if itemEmbeddings == None:
    #             itemEmbeddings = itemEmbedding
    #         else:
    #             itemEmbeddings += itemEmbedding
    #
    #     item_embeddings = tf.reshape(tf.nn.embedding_lookup(weights, test_item_ids),
    #                                        [-1, self.numFactor])

    def cal_rating_loss(self, output, label, mask):
        if self.lossType == 'mae':
            return tf.nn.l2_loss((label - output) * mask)
        if self.lossType == 'entropy':
            return tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(labels=label, logits=output * mask))

    def softmask(self, mask, attentions):

        attention_mask = tf.where(mask, attentions, tf.ones(shape=[self.batch_num, self.item_pad_num]) * np.NINF)
        attention_mask = tf.nn.softmax(attention_mask)

        return attention_mask