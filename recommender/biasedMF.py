import numpy as np
import tensorflow as tf
import random
import recommender.BasicRcommender

class BiasedMFRecommender(recommender.BasicRcommender.BasicRecommender):

    def __init__(self, dataModel, config):

        super(BiasedMFRecommender, self).__init__(dataModel, config)


        self.user_reviewVectors = dataModel.user_reviewVectors

        self.name = 'BiasedMFRecommender'

        self.numFactor = config['numFactor']
        self.lam1 = config['lam1']

        # placeholders
        self.u_id = tf.placeholder(tf.int32, [None, 1])
        self.i_id = tf.placeholder(tf.int32, [None, 1])
        self.r_label = tf.placeholder(tf.float32, [None, 1])

        # user/item embedding
        tf.set_random_seed(123)
        random.seed(123)
        self.userEmbedding = tf.Variable(tf.random_uniform([self.numUser, self.numFactor], -0.1, 0.1))
        self.itemEmbedding = tf.Variable(tf.random_uniform([self.numItem, self.numFactor], -0.1, 0.1))

        self.u_bias = tf.Variable(tf.random_uniform([self.numUser, 1], -0.1, 0.1))
        self.i_bias = tf.Variable(tf.random_uniform([self.numItem, 1], -0.1, 0.1))
        self.globalMean = np.mean(self.trainSet[:, 2:3])

    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:
            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])
            itemEmedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, self.i_id), [-1, self.numFactor])

            u_bias = tf.reshape(tf.nn.embedding_lookup(self.u_bias, self.u_id), [-1, 1])
            i_bias = tf.reshape(tf.nn.embedding_lookup(self.i_bias, self.i_id), [-1, 1])


            self.r_pred = tf.reduce_sum(tf.multiply(userEmedding, itemEmedding), 1, keep_dims=True) + u_bias + i_bias + self.globalMean

        l2_norm = self.lam1 * tf.nn.l2_loss(self.userEmbedding) \
                   + self.lam1 * tf.nn.l2_loss(self.itemEmbedding) \
                  + self.lam1 * tf.nn.l2_loss(self.u_bias) \
                  + self.lam1 * tf.nn.l2_loss(self.i_bias) \

        ratingLoss = tf.reduce_sum(tf.square(self.r_pred - self.r_label))

        self.cost = 0.5 * (ratingLoss + l2_norm)

    def trainEachBatch(self, epochId, batchId):
        batch_u, batch_v, batch_r = self.getTrainData(batchId)
        self.optimizer.run(feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
            self.r_label: batch_r
        })
        loss = self.cost.eval(feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
            self.r_label: batch_r
        })

        self.logger.info(
            "batchId: %d epoch %d/%d   loss: %.4f" % (batchId, epochId, self.maxIter, loss))
        if self.goal == 'ranking':
            self.evaluateRanking(epochId, batchId)
        elif self.goal == 'rating':
            self.evaluateRating(epochId, batchId)
        else:
            self.evaluateRanking(epochId, batchId)
            self.evaluateRating(epochId, batchId)

    def getTrainData(self, batchId):
        # compute start and end
        start = batchId * self.trainBatchSize
        end = start + self.trainBatchSize
        if end > self.trainSize:
            end = self.trainSize

        batch_u = self.trainSet[start:end, 0:1].astype(np.int32)
        batch_v = self.trainSet[start:end, 1:2].astype(np.int32)
        batch_r = self.trainSet[start:end, 2:3]

        return batch_u, batch_v, batch_r

    def getRatingPredictions(self):
        batch_u = self.testSet[:, 0:1].astype(np.int32)
        batch_v = self.testSet[:, 1:2].astype(np.int32)

        predList = self.r_pred.eval(feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v
        })

        for i in range(len(predList)):
            if predList[i][0] > 5:
                predList[i][0] = 5
            if predList[i][0] < 1:
                predList[i][0] = 1
        return predList


    def getPredList_ByUserIdx(self, userIdx):
        # build test batch
        testBatch_user = []

        for itemIdx in self.evalItemsForEachUser[userIdx]:
            testBatch_user.append([userIdx, itemIdx, 1.0])

        batch_u = np.array(testBatch_user)[:, 0:1]
        batch_v = np.array(testBatch_user)[:, 1:2]

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
        })

        recommendList = {}
        for i in range(len(testBatch_user)):
            recommendList[batch_v[i][0]] = predList[i][0]

        sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]

        return sorted_RecItemList












