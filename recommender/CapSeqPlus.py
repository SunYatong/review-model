import numpy as np
import tensorflow as tf
import random
from recommender.BasicRcommender import BasicRecommender
import time
from component.Conv import CNN_Compoment
from component.Capsule import Capsule_Component
from component.MLP import MLP

class CapSeqPlusRecommender(BasicRecommender):

    def __init__(self, dataModel, config):

        super(CapSeqPlusRecommender, self).__init__(dataModel, config)

        self.train_users = dataModel.train_users
        self.train_sequences_input = dataModel.train_sequences_input
        self.train_sequences_target = dataModel.train_sequences_target
        self.user_pred_sequences = dataModel.user_pred_sequences

        self.trainSize = len(self.train_users)
        self.trainBatchNum = int(self.trainSize // self.trainBatchSize) + 1

        self.name = 'CapSeqPlus'

        self.numFactor = config['numFactor']
        self.factor_lambda = config['factor_lambda']
        self.input_length = config['input_length']
        self.input_image_height = self.input_length + 1
        self.target_length = config['target_length']
        self.filter_num = config['filter_num']
        self.dropout_keep = config['dropout_keep']
        self.capsule_num = config['capsule_num']
        self.dynamic_routing_iter = config['dynamic_routing_iter']
        self.loss = config['loss']

        # placeholders
        self.u_id = tf.placeholder(tf.int32, [self.trainBatchSize, 1])
        self.input_seq = tf.placeholder(tf.int32, [self.trainBatchSize, self.input_length])
        self.target_seq_pos = tf.placeholder(tf.int32, [self.trainBatchSize, self.target_length])
        self.target_seq_neg = tf.placeholder(tf.int32, [self.trainBatchSize, self.target_length])
        self.pred_seq = tf.placeholder(tf.int32, [self.trainBatchSize, 100])
        self.dropout_keep_placeholder = tf.placeholder_with_default(1.0, shape=())

        # user/item embedding
        self.userEmbedding = tf.Variable(tf.random_normal([self.numUser, self.numFactor], 0, 0.1))
        self.itemEmbedding = tf.Variable(tf.random_normal([self.numItem, self.numFactor], 0, 0.1))
        self.itemBias = tf.Variable(tf.random_normal([self.numItem], 0, 0.1))

        # Conv-Capsule Layers
        self.hor_convs = []
        self.hor_pattern_capsules = []
        self.user_embeddings = []
        for i in range(self.input_image_height):
            filter_height = i + 1
            conv = CNN_Compoment(
                filter_num=self.filter_num,
                filter_height=filter_height,
                filter_width=self.numFactor,
                name='hor'
            )
            capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.filter_num,
                num_caps_j=self.capsule_num,
                in_vec_len=self.input_image_height - filter_height + 1,
                out_vec_len=self.input_image_height - filter_height + 1,
                user_vec_len=self.numFactor,
                user_bias=True,
                T=self.dynamic_routing_iter,
                name='pattern'
            )
            self.hor_convs.append(conv)
            self.hor_pattern_capsules.append(capsule)

        # time capsule
        self.time_capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.input_image_height,
                num_caps_j=self.capsule_num,
                in_vec_len=self.numFactor,
                out_vec_len=self.numFactor,
                user_vec_len=self.numFactor,
                user_bias=True,
                T=self.dynamic_routing_iter,
                name='time'
            )

        self.concate_size = (((
                            self.input_image_height + 1) * self.input_image_height) / 2 + self.numFactor) * self.capsule_num + self.numFactor
        self.fc_dim = [self.concate_size] + config['item_fc_dim']
        self.mlp = MLP(self.fc_dim, dropout_keep=self.dropout_keep_placeholder)
        self.rating_matrix = tf.Variable(tf.random_normal([config['item_fc_dim'][-1], 1], 0, 0.1))

    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:
            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])

            pos_preds, input_image_pos = self.get_pred(input_item_ids=self.input_seq,
                                      target_item_ids=self.target_seq_pos,
                                      userEmedding=userEmedding,
                                      tar_length=self.target_length)
            neg_preds, input_image_neg = self.get_pred(input_item_ids=self.input_seq,
                                      target_item_ids=self.target_seq_neg,
                                      userEmedding=userEmedding,
                                      tar_length=self.target_length)
            if self.loss == 'bpr':
                rating_loss = - tf.reduce_sum(tf.log(tf.sigmoid(pos_preds - neg_preds)))
            else:
                rating_loss = - tf.reduce_mean(tf.log(tf.nn.sigmoid(pos_preds))) - tf.reduce_mean(tf.log(1 - tf.nn.sigmoid(neg_preds)))

            l2_loss = self.factor_lambda * (tf.nn.l2_loss(userEmedding) + tf.nn.l2_loss(input_image_pos) + tf.nn.l2_loss(input_image_neg))

            self.cost = rating_loss + l2_loss

            self.r_pred, image = self.get_pred(input_item_ids=self.input_seq,
                                        target_item_ids=self.pred_seq,
                                        userEmedding=userEmedding,
                                        tar_length=100)

    def get_pred(self, input_item_ids, target_item_ids, userEmedding, tar_length):

        split_list = [1] * tar_length
        target_item_id_list = tf.split(target_item_ids, split_list, 1)
        preds = []

        for target_item_id in target_item_id_list:
            image_input_ids = tf.concat([input_item_ids, target_item_id], axis=1)
            input_image = tf.nn.embedding_lookup(self.itemEmbedding, image_input_ids)
            input_image = tf.reshape(input_image, [-1, self.input_image_height, self.numFactor])
            input_image = tf.expand_dims(input_image, -1)
            # assert input_image.get_shape() == [-1, self.input_length, self.numFactor, 1]

            embeddings = []
            for i in range(self.input_image_height):
                filter_height = i + 1
                conv = self.hor_convs[i].get_output(input_image)
                # assert conv.get_shape() == [-1, self.input_length-filter_height+1, 1, self.filter_num]

                conv = tf.transpose(conv, perm=[0, 3, 1, 2])
                # assert conv.get_shape() == [-1, self.filter_num, self.input_length-filter_height+1, 1]

                # assert conv.get_shape() == [-1, self.input_length - filter_height + 1, self.filter_num, 1]
                # raw capsule output shape = [batch_size, num_j_capsule, output_vec_len, 1]
                output = self.hor_pattern_capsules[i].get_output(conv, userEmedding)
                # assert output.get_shape() == [-1, self.input_length - filter_height + 1]
                output = tf.reshape(output, [-1, self.capsule_num * (self.input_image_height - filter_height + 1)])
                embeddings.append(output)

            # raw capsule output shape = [batch_size, num_j_capsule, output_vec_len, 1]
            time_output = self.time_capsule.get_output(input_image, userEmedding)
            time_output = tf.reshape(time_output, [-1, self.capsule_num * self.numFactor])
            # assert time_output.get_shape() == [-1, capsule_num * self.numFactor]
            embeddings.append(time_output)
            embeddings.append(userEmedding)

            concate_vec = tf.concat(values=embeddings, axis=1)
            # assert concate_vec.get_shape() == [-1, self.concate_size]
            merged_embed = self.mlp.get_output(feature_input=concate_vec)
            # assert merged_embed.get_shape() == [-1, self.numFactor]

            pred_rating = tf.matmul(merged_embed, self.rating_matrix)
            preds.append(pred_rating)

        return tf.concat(preds, axis=1), input_image

    def trainEachBatch(self, epochId, batchId):
        totalLoss = 0
        start = time.time()
        feed_dict = self.getTrainData(batchId)

        self.optimizer.run(feed_dict=feed_dict)
        loss = self.cost.eval(feed_dict=feed_dict)

        totalLoss += loss
        end = time.time()
        if epochId % 5 == 0 and batchId == 0:
            self.logger.info("----------------------------------------------------------------------")
            self.logger.info(
                "batchId: %d epoch %d/%d   batch_loss: %.4f   time of a batch: %.4f" % (
                    batchId, epochId, self.maxIter, totalLoss, (end - start)))

            self.evaluateRanking(epochId, batchId)
        return totalLoss

    def getTrainData(self, batchId):
        # compute start and end
        start = time.time()
        neg_seq_batch = []

        start_idx = batchId * self.trainBatchSize
        end_idx = start_idx + self.trainBatchSize

        if end_idx > self.trainSize:
            end_idx = self.trainSize
            start_idx = end_idx - self.trainBatchSize

        if end_idx == start_idx:
            start_idx = 0
            end_idx = start_idx + self.trainBatchSize

        user_batch = self.train_users[start_idx:end_idx]
        input_seq_batch = self.train_sequences_input[start_idx:end_idx]
        pos_seq_batch = self.train_sequences_target[start_idx:end_idx]

        for userIdx in user_batch:
            neg_items = []
            for i in range(self.target_length):
                positiveItems = self.user_items_train[userIdx]
                negativeItemIdx = random.randint(0, self.numItem - 1)
                while negativeItemIdx in positiveItems:
                    negativeItemIdx = random.randint(0, self.numItem - 1)
                neg_items.append(negativeItemIdx)
            neg_seq_batch.append(neg_items)

        user_batch = np.array(user_batch).reshape((end_idx - start_idx, 1))
        input_seq_batch = np.array(input_seq_batch)
        pos_seq_batch = np.array(pos_seq_batch)
        neg_seq_batch = np.array(neg_seq_batch)

        end = time.time()
        # self.logger.info("time of collect a batch of data: " + str((end - start)) + " seconds")
        # self.logger.info("batch Id: " + str(batchId))
        feed_dict = {
            self.u_id: user_batch,
            self.input_seq: input_seq_batch,
            self.target_seq_pos: pos_seq_batch,
            self.target_seq_neg: neg_seq_batch,
            self.dropout_keep_placeholder: self.dropout_keep
        }

        return feed_dict

    def getPredList_ByUserIdxList(self, user_idices):
        end0 = time.time()
        # build test batch
        input_seq = []
        target_seq = []

        for userIdx in user_idices:
            input_seq.append(self.user_pred_sequences[userIdx])
            target_seq.append(self.evalItemsForEachUser[userIdx])

        batch_u = np.array(user_idices).reshape((-1, 1))
        input_seq = np.array(input_seq)
        target_seq = np.array(target_seq)

        end1 = time.time()

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_id: batch_u,
            self.input_seq: input_seq,
            self.pred_seq: target_seq,
        })
        end2 = time.time()

        output_lists = []
        for i in range(len(user_idices)):
            recommendList = {}
            start = i * 100
            end = start + 100
            for j in range(end-start):
                recommendList[target_seq[i][j]] = predList[i][j]
            sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
            output_lists.append(sorted_RecItemList)
        end3 = time.time()

        return output_lists, end1 - end0, end2 - end1, end3 - end2
