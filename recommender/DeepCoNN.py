from recommender.ReviewRecommender import ReviewRecommender
import tensorflow as tf
from component.Conv_Pool import CNN_Pool_Compoment
from component.FM import FM
import numpy as np
import random
import time

class DeepCoNN(ReviewRecommender):

    def __init__(self, dataModel, config):

        super(DeepCoNN, self).__init__(dataModel, config)
        self.name = 'DeepCoNN'
        self.item_reviewVectors = dataModel.item_reviewVectors
        self.user_reviewVectors = dataModel.user_reviewVectors

        self.cnn_lambda = config['review_lambda']
        self.filter_sizes = config['filters_sizes']
        self.num_filters = config['num_filters']
        self.fm_k = config['fm_k']

        self.review_network = CNN_Pool_Compoment(
            filter_num=self.num_filters,
            filter_sizes=self.filter_sizes,
            output_size=self.numFactor,
            wordvec_size=self.wordVec_size,
            max_review_length=self.maxReviewLength,
            word_matrix=self.word_embedding_matrix,
            review_wordId_print=self.review_wordId_print,
            review_input_print=self.review_input_print,
            cnn_lambda=self.cnn_lambda,
            dropout_keep_prob=self.dropout_keep_prob,
            component_raw_output=self.component_raw_output,
            item_pad_num=self.item_pad_num,
            name='hor',
        )

        self.user_review_input = tf.placeholder(tf.int32, [None, self.maxReviewLength],
                                                         name="user_review_input")

        self.positive_item_review_input = tf.placeholder(tf.int32, [None, self.maxReviewLength],
                                                         name="positive_item_review_input")
        self.negative_item_review_input = tf.placeholder(tf.int32, [None, self.maxReviewLength],
                                                         name="negative_item_review_input")

        self.FM = FM(feature_size=self.numFactor * 2, fm_k=self.fm_k)

    def buildModel(self):
        user_review_embed   = self.review_network.get_single_output(self.user_review_input)
        item_review_embed_i = self.review_network.get_single_output(self.positive_item_review_input)
        item_review_embed_j = self.review_network.get_single_output(self.negative_item_review_input)

        pos_concat = tf.concat([user_review_embed, item_review_embed_i], axis=1, name='pos_concat')
        neg_concat = tf.concat([user_review_embed, item_review_embed_j], axis=1, name='pos_concat')

        pos_concat_drop = tf.nn.dropout(pos_concat, self.dropout_keep_prob)
        neg_concat_drop = tf.nn.dropout(neg_concat, self.dropout_keep_prob)

        pos_rating = self.FM.get_output(feature_input=pos_concat_drop)
        neg_rating = self.FM.get_output(feature_input=neg_concat_drop)

        predDiff = pos_rating - neg_rating
        bprLoss = - tf.reduce_sum(tf.log(tf.sigmoid(predDiff)))

        self.r_pred = pos_rating
        self.cost = bprLoss

    def getTrainData(self, batchId):

        start = time.time()

        user_reviewBatch = []
        pos_item_review = []
        neg_item_review = []

        for i in range(self.trainBatchSize):
            userIdx = random.randint(0, self.numUser - 1)
            positiveItems = self.user_items_train[userIdx]
            random_idx = random.randint(0, len(positiveItems) - 1)
            positiveItemIdx = positiveItems[random_idx]
            negativeItemIdx = random.randint(0, self.numItem - 1)
            while negativeItemIdx in positiveItems:
                negativeItemIdx = random.randint(0, self.numItem - 1)

            user_reviewBatch.append(self.user_reviewVectors[userIdx])
            pos_item_review.append(self.item_reviewVectors[positiveItemIdx])
            neg_item_review.append(self.item_reviewVectors[negativeItemIdx])

        user_reviewBatch = np.array(user_reviewBatch)
        pos_item_review = np.array(pos_item_review)
        neg_item_review = np.array(neg_item_review)

        end = time.time()

        return user_reviewBatch, pos_item_review, neg_item_review, str((end - start))

    def trainEachBatch(self, epochId, batchId):
        start = time.time()
        print_flag = 1
        batch_u_reviews, pos_item_review, neg_item_review, time_cost = self.getTrainData(
            batchId)
        self.optimizer.run(feed_dict={
            self.user_review_input: batch_u_reviews,
            self.positive_item_review_input: pos_item_review,
            self.negative_item_review_input: neg_item_review,
            self.dropout_keep_prob: self.dropout_keep_prob_input,
        })
        ratingLoss = self.cost.eval(feed_dict={
            self.user_review_input: batch_u_reviews,
            self.positive_item_review_input: pos_item_review,
            self.negative_item_review_input: neg_item_review,
            self.dropout_keep_prob: self.dropout_keep_prob_input,
        })

        loss = ratingLoss

        end = time.time()

        if epochId % 5 == 0 and batchId == 0:
            self.logger.info("----------------------------------------------------------------------")
            self.logger.info("time of collect a batch of data: " + time_cost + " seconds")
            self.logger.info(
                "rating_loss: %.4f   factor_l2_loss: %.4f   cnn_l2_loss: %.4f " % (
                    ratingLoss, 0.0, 0.0))
            self.logger.info(
                "batchId: %d epoch %d/%d   batch_loss: %.4f   time of a batch: %.4f" % (
                    batchId, epochId, self.maxIter, loss, (end - start)))
            self.evaluateRanking(epochId, batchId)
        return loss

    def getPredList_ByUserIdxList(self, user_idices):
        # build test batch
        end0 = time.time()
        testBatch_user = []
        user_review = []
        item_review = []

        for i in range(len(user_idices)):
            userIdx = user_idices[i]
            for itemIdx in self.evalItemsForEachUser[userIdx]:
                testBatch_user.append([userIdx, itemIdx, 1.0])
                item_review.append(self.item_reviewVectors[itemIdx])
            batch_u_reviews = [self.user_reviewVectors[userIdx]] * 100
            user_review.extend(batch_u_reviews)

        batch_u = np.array(testBatch_user)[:, 0:1].astype(np.int32)
        batch_v = np.array(testBatch_user)[:, 1:2].astype(np.int32)
        user_review = np.array(user_review)
        item_review = np.array(item_review)
        end1 = time.time()

        predList = self.sess.run(self.r_pred, feed_dict={
            self.user_review_input: user_review,
            self.positive_item_review_input: item_review
        })
        end2 = time.time()

        output_lists = []
        for i in range(len(user_idices)):
            recommendList = {}
            start = i * 100
            end = start + 100
            for j in range(start, end):
                recommendList[batch_v[j][0]] = predList[j][0]
            sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
            output_lists.append(sorted_RecItemList)

        end3 = time.time()

        return output_lists, end1 - end0, end2 - end1, end3 - end2







