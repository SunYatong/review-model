import numpy as np
import tensorflow as tf
import random
import time
from component.MLP import MLP
from recommender.PMF import PMFRecommender


class WideDeep(PMFRecommender):
    def __init__(self, dataModel, config):

        super(WideDeep, self).__init__(dataModel, config)

        self.name = 'WideDeep'
        self.featureSize = dataModel.featureSize
        self.type = config['type']
        self.dims = [self.featureSize] + config['dims']
        self.negative = config['negative']
        self.maxHist = dataModel.maxHist
        self.user_features = dataModel.user_features

        self.u_feature = tf.placeholder(tf.float32, [None, self.featureSize])
        self.r_label = tf.placeholder(tf.float32, [None, 1])
        self.item_id = tf.placeholder(tf.int32, [None, 1])
        self.u_items_ids = tf.placeholder(tf.int32, [None, self.maxHist])

        self.wide_weight = tf.Variable(tf.random_uniform([self.featureSize + 2*self.numFactor, 1], -0.1, 0.1))
        self.itemEmbedding = tf.Variable(tf.random_uniform([self.numItem + 1, self.numFactor], -0.1, 0.1))
        self.deep_network = MLP(self.dims, dropout_keep=0.8)

        self.user_feature_vecs_eval, self.user_item_vecs_eval = self.get_user_vectors_for_eval()

    def buildModel(self):

        curr_item = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, self.item_id), [-1, self.numFactor])
        hist_items = tf.nn.embedding_lookup(self.itemEmbedding, self.u_items_ids)
        hist_items = tf.reshape(tf.reduce_sum(hist_items, axis=[1]), [-1, self.numFactor])
        input_feature = tf.concat([self.u_feature, hist_items, curr_item], axis=1)

        if self.type == 'wide':
            wide_output = tf.matmul(input_feature, self.wide_weight, name='wide_output')
            self.r_pred = wide_output

        if self.type == 'deep':
            deep_output = self.deep_network.get_output(input_feature)
            self.r_pred = deep_output

        if self.type == 'wide-deep':
            wide_output = tf.matmul(input_feature, self.wide_weight, name='wide_output')
            deep_output = self.deep_network.get_output(self.u_feature)
            self.r_pred = wide_output + deep_output

        ratingLoss = tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(labels=self.r_label, logits=self.r_pred))
        L1_norm = 0.001 * tf.reduce_sum(tf.abs(self.wide_weight))
        self.cost = ratingLoss + L1_norm

    def trainEachBatch(self, epochId, batchId):
        start = time.time()
        totalLoss = 0
        print_flag = 1
        batch_u_feature, batch_v, user_items_batch, batch_r = self.getTrainData(batchId)
        self.optimizer.run(feed_dict={
            self.u_feature: batch_u_feature,
            self.item_id: batch_v,
            self.u_items_ids: user_items_batch,
            self.r_label: batch_r
        })
        loss = self.cost.eval(feed_dict={
            self.u_feature: batch_u_feature,
            self.item_id: batch_v,
            self.u_items_ids: user_items_batch,
            self.r_label: batch_r
        })
        totalLoss += loss


        end = time.time()

        if epochId % 5 == 0 and batchId == 0:
            self.logger.info("----------------------------------------------------------------------")
            self.logger.info(
                "batchId: %d epoch %d/%d   batch_loss: %.4f   time of a batch: %.4f" % (
                    batchId, epochId, self.maxIter, totalLoss, (end - start)))
            self.evaluateRanking(epochId, batchId)
        return totalLoss

    def getTrainData(self, batchId):

        # compute start and end
        start = batchId * self.trainBatchSize
        end = start + self.trainBatchSize
        if end > self.trainSize:
            end = self.trainSize
        batch_u = self.trainSet[start:end, 0:1].astype(np.int32)
        batch_v = self.trainSet[start:end, 1:2].astype(np.int32)
        batch_r = self.trainSet[start:end, 2:3]
        batch_u_feature = []
        user_items_batch = []

        for user in batch_u:
            userIdx = user[0]
            batch_u_feature.append(self.user_features[userIdx])
            user_items_batch.append(self.user_items_train[userIdx])


        if self.negative > 0:
            rankBatch = self.trainSet[start:end]
            for user in batch_u:
                userIdx = user[0]
                itemsInTrain = self.user_items_train[userIdx]
                for negativeNum in range(self.trainBatchSize * self.negative):
                    newItemIdx = random.randint(0, self.numItem - 1)
                    while newItemIdx in itemsInTrain:
                        newItemIdx = random.randint(0, self.numItem - 1)
                    rankBatch = np.append(rankBatch, [[userIdx, newItemIdx, 0.0]], 0)
                batch_u_feature.append(self.user_features[userIdx])
                user_items_batch.append(self.user_items_train[userIdx])

            batch_u = rankBatch[:, 0:1].astype(np.int32)
            batch_v = rankBatch[:, 1:2].astype(np.int32)
            batch_r = rankBatch[:, 2:3]
            batch_u_feature = np.array(batch_u_feature)
            user_items_batch = np.array(user_items_batch)

        return batch_u_feature, batch_v, user_items_batch, batch_r

    def getPredList_ByUserIdxList(self, user_idices):
        # build test batch
        end0 = time.time()
        testBatch_user = []
        batch_u_features_total = []
        batch_u_items_total = []
        batch_num = len(user_idices) * 100

        for i in range(len(user_idices)):
            userIdx = user_idices[i]
            for itemIdx in self.evalItemsForEachUser[userIdx]:
                testBatch_user.append([userIdx, itemIdx, 1.0])
            batch_u_features = self.user_feature_vecs_eval[userIdx]
            batch_u_features_total.extend(batch_u_features)
            batch_u_items = self.user_item_vecs_eval[userIdx]
            batch_u_items_total.extend(batch_u_items)

        batch_v = np.array(testBatch_user)[:, 1:2].astype(np.int32)
        batch_u_features_total = np.array(batch_u_features_total)
        batch_u_items_total = np.array(batch_u_items_total)
        end1 = time.time()

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_feature: batch_u_features_total,
            self.item_id: batch_v,
            self.u_items_ids: batch_u_items_total,
        })

        end2 = time.time()

        output_lists = []
        for i in range(len(user_idices)):
            recommendList = {}
            start = i * 100
            end = start + 100
            for j in range(start, end):
                recommendList[batch_v[j][0]] = predList[j][0]
            sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
            output_lists.append(sorted_RecItemList)

        end3 = time.time()

        return output_lists, end1-end0, end2-end1, end3-end2

    def getPredList_ByUserIdx(self, userIdx):

        # build test batch
        testBatch_user = []

        for itemIdx in self.evalItemsForEachUser[userIdx]:
            testBatch_user.append([userIdx, itemIdx, 1.0])

        batch_u = np.array(testBatch_user)[:, 0:1]
        batch_v = np.array(testBatch_user)[:, 1:2]

        batch_u_features = [self.user_features[userIdx] for i in range(len(testBatch_user))]
        batch_u_items = [self.user_items_train[userIdx] for i in range(len(testBatch_user))]

        batch_u_features = np.array(batch_u_features)
        batch_u_items = np.array(batch_u_items)

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
            self.u_items_ids: batch_u_items,
        })

        recommendList = {}
        for i in range(len(testBatch_user)):
            recommendList[batch_v[i][0]] = predList[i][0]

        sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
        # print("user " + str(userIdx) + "/" + str(self.numUser-1) + " finished")

        return sorted_RecItemList

    def get_user_vectors_for_eval(self):
        user_feature_vecs = {}
        user_itemVecs = {}
        for userIdx in self.user_items_test:
            user_feature_vecs[userIdx] = [self.user_features[userIdx]] * 100
            user_itemVecs[userIdx] = [self.user_items_train[userIdx]] * 100
        return user_feature_vecs, user_itemVecs


















