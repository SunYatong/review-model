from recommender.BasicRcommender import BasicRecommender
import numpy as np
import tensorflow as tf
import random
import time

class ReviewRecommender(BasicRecommender):

    def __init__(self, dataModel, config):
        self.user_items_reviewVectors = dataModel.user_items_reviewVectors
        self.maxReviewLength = dataModel.maxReviewLength
        self.user_review_mask = dataModel.user_review_mask

        super(ReviewRecommender, self).__init__(dataModel, config)
        self.numFactor = config['numFactor']
        self.wordVec_size = config['wordVecSize']
        self.factor_lambda = config['factor_lambda']
        self.atten_lambda = config['atten_lambda']
        self.dropout_keep_prob_input = config['dropout_keep_prob']
        self.attention_version = config['attention_version']
        self.verbose = config['verbose']
        self.item_pad_num = config['item_pad_num']

        # placeholders
        self.u_id = tf.placeholder(tf.int32, [None, 1])
        self.i_id = tf.placeholder(tf.int32, [None, 1])
        self.j_id = tf.placeholder(tf.int32, [None, 1])
        self.u_items_ids = tf.placeholder(tf.int32, [None, self.item_pad_num])
        self.review_input = tf.placeholder(tf.int32, [None, self.item_pad_num, self.maxReviewLength], name="raw_review_input") # shape:[batchSize, reviewNum, sequence_length]
        self.dropout_keep_prob = tf.placeholder_with_default(1.0, shape=())

        self.u_pos_items_ids_mask = tf.placeholder(tf.bool, [None, self.item_pad_num])
        self.u_pos_items_ids_mask_print = tf.Print(input_=self.u_pos_items_ids_mask, data=[self.u_pos_items_ids_mask], message="u_pos_items_ids_mask",
                                                   summarize=self.item_pad_num)

        self.u_neg_items_ids_mask = tf.placeholder(tf.bool, [None, self.item_pad_num])
        self.u_neg_items_ids_mask_print = tf.Print(input_=self.u_neg_items_ids_mask, data=[self.u_neg_items_ids_mask],
                                                   message="u_neg_items_ids_mask",
                                                   summarize=self.item_pad_num)

        self.batch_num = tf.placeholder(tf.int32, shape=())

        # user/item embedding
        with tf.device('/cpu:0'), tf.name_scope("embedding"):
            self.userEmbedding = tf.Variable(tf.random_normal([self.numUser, self.numFactor], 0, 0.01))
            self.itemEmbedding = tf.Variable(tf.random_normal([self.numItem, self.numFactor], 0, 0.01))

        # word embedding matrix
        self.word_embedding_matrix = tf.constant(name="word_embedding_matrix",
                                                 shape=[self.numWord, self.wordVec_size],
                                                 dtype=tf.float32,
                                                 value=dataModel.wordMatrix)

        # item attention parameters
        with tf.variable_scope("item_attention_weights", reuse=None):
            user_W = tf.get_variable(name="user_W", shape=[self.numFactor, self.numFactor // 2], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())
            item_W = tf.get_variable(name="item_W", shape=[self.numFactor, self.numFactor // 2], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())
            review_W = tf.get_variable(name="review_W", shape=[self.numFactor, self.numFactor // 2], dtype=tf.float32,
                                       initializer=tf.contrib.layers.xavier_initializer())
            embed_bias = tf.get_variable(name="embed_bias", dtype=tf.float32,
                                        initializer=tf.constant(0.1, shape=[self.numFactor // 2],))
            summary_W = tf.get_variable(name="summary_W", shape=[self.numFactor // 2, 1], dtype=tf.float32,
                                        initializer=tf.contrib.layers.xavier_initializer())
            summary_b = tf.get_variable(name="summary_b", dtype=tf.float32,
                                        initializer=tf.constant(0.1))

        # feature attention parameters
        with tf.variable_scope("feature_attention_weights", reuse=None):
            W = tf.get_variable(name="feature_atten_W", shape=[self.numFactor * 4, self.numFactor], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())
            b = tf.get_variable(name="feature_atten_b", shape=[self.numFactor], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())

        self.atten_reg = self.atten_lambda * (tf.nn.l2_loss(user_W) + tf.nn.l2_loss(item_W) + tf.nn.l2_loss(review_W) + tf.nn.l2_loss(summary_W))

        self.sampleSize = self.trainSize
        self.user_review_vecs_eval, self.user_item_vecs_eval = self.get_user_vectors_for_eval()

        self.factor_l2_loss = None
        self.review_l2_loss = None
        self.rating_loss = None

        self.attentionPrint_i = None
        self.attentionPrint_j = None
        self.u_id_print = None
        self.positive_i_print = None
        self.negative_j_print = None
        self.item_ids_print = None
        self.atten_i_print = None
        self.atten_j_print = None
        self.review_input_print = [n for n in range(self.item_pad_num)]
        self.review_wordId_print = [n for n in range(self.item_pad_num)]
        self.review_output_print = [n for n in range(self.item_pad_num)]
        self.review_atten_print = [n for n in range(self.item_pad_num)]
        self.component_raw_output = [n for n in range(self.item_pad_num)]
        self.review_network = None

    def buildModel(self):

        with tf.variable_scope(tf.get_variable_scope()) as scope:
            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])
            itemEmedding_i = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, self.i_id), [-1, self.numFactor])
            itemEmedding_j = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, self.j_id), [-1, self.numFactor])

            component_outputs = self.review_network.get_outputs(self.review_input)
            component_l2Loss = self.review_network.get_l2_loss()
            if self.attention_version == 'old':
                attention_outputs_i = self.attentionOutputs(userEmedding, itemEmedding_i, flag='p')
                attention_outputs_j = self.attentionOutputs(userEmedding, itemEmedding_j, flag='n')
            else:
                attention_outputs_i = self.new_attention_output(userEmedding, itemEmedding_i, component_outputs, 'p')
                attention_outputs_j = self.new_attention_output(userEmedding, itemEmedding_j, component_outputs, 'n')

            reviewOutputs_i = component_outputs[0] * attention_outputs_i[0]
            reviewOutputs_j = component_outputs[0] * attention_outputs_j[0]
            for index in range(1, len(component_outputs)):
                reviewOutputs_i += component_outputs[index] * attention_outputs_i[index]
                reviewOutputs_j += component_outputs[index] * attention_outputs_j[index]
            reviewOutputs_i += userEmedding
            reviewOutputs_j += userEmedding

            pred_i = tf.reduce_sum(tf.multiply(reviewOutputs_i, itemEmedding_i), 1, keep_dims=True)
            pred_j = tf.reduce_sum(tf.multiply(reviewOutputs_j, itemEmedding_j), 1, keep_dims=True)

            predDiff = pred_i - pred_j

            self.rating_loss = - tf.reduce_sum(tf.log(tf.sigmoid(predDiff)))

            self.r_pred = pred_i

        self.factor_l2_loss = self.factor_lambda * tf.nn.l2_loss(userEmedding) \
                   + self.factor_lambda * tf.nn.l2_loss(itemEmedding_i)\
                   + self.factor_lambda * tf.nn.l2_loss(itemEmedding_j)\

        self.review_l2_loss = component_l2Loss

        self.cost = self.rating_loss + self.factor_l2_loss + self.review_l2_loss
        if self.attention_version != 'old':
            self.cost += self.atten_reg

        self.u_id_print = tf.Print(input_=self.u_id, data=[self.u_id], message="user_idx", summarize=1)
        self.positive_i_print = tf.Print(input_=self.i_id, data=[self.i_id], message="positive_item_idx", summarize=1)
        self.negative_j_print = tf.Print(input_=self.j_id, data=[self.j_id], message="negative_item_idx", summarize=1)
        self.item_ids_print = tf.Print(input_=self.u_items_ids, data=[self.u_items_ids], message="hist_items_idx", summarize=self.item_pad_num)
        for i in range(self.item_pad_num):
            self.review_output_print[i] = tf.Print(component_outputs[i], [component_outputs[i]], message="review_output_%d" % (i),
                                                   summarize=self.numFactor * 2)

    def getTrainData(self, batchId):
        start = time.time()

        user_batch = []
        pos_item_batch = []
        neg_item_batch = []
        reviewBatch = []
        user_items_batch = []
        user_items_mask_pos = []
        user_items_mask_neg = []

        for i in range(self.trainBatchSize):
            userIdx = random.randint(0, self.numUser - 1)
            positiveItems = self.user_items_train[userIdx]
            positiveItems_paded = self.user_items_train_paded[userIdx]

            positiveItems_mask = list(self.user_review_mask[userIdx])
            negativeItems_mask = list(self.user_review_mask[userIdx])

            random_idx = random.randint(0, len(positiveItems) - 1)
            positiveItems_mask[random_idx] = False
            positiveItemIdx = positiveItems[random_idx]
            negativeItemIdx = random.randint(0, self.numItem - 1)
            while negativeItemIdx in positiveItems:
                negativeItemIdx = random.randint(0, self.numItem - 1)
            reviewVectors = self.user_items_reviewVectors[userIdx]

            user_batch.append(userIdx)
            pos_item_batch.append(positiveItemIdx)
            neg_item_batch.append(negativeItemIdx)
            reviewBatch.append(reviewVectors)
            user_items_batch.append(positiveItems_paded)
            user_items_mask_pos.append(positiveItems_mask)
            user_items_mask_neg.append(negativeItems_mask)

        batch_u = np.array(user_batch).reshape((self.trainBatchSize, 1))
        batch_i = np.array(pos_item_batch).reshape((self.trainBatchSize, 1))
        batch_j = np.array(neg_item_batch).reshape((self.trainBatchSize, 1))
        user_items_batch = np.array(user_items_batch)
        reviewBatch = np.array(reviewBatch)
        user_items_mask_pos = np.array(user_items_mask_pos)
        user_items_mask_neg = np.array(user_items_mask_neg)

        end = time.time()

        return batch_u, batch_i, batch_j, user_items_batch, reviewBatch, user_items_mask_pos, user_items_mask_neg, str((end - start))

    def trainEachBatch(self, epochId, batchId):
        start = time.time()
        print_flag = 1
        batch_u, batch_v, batch_j, batch_u_items, batch_u_reviews, user_items_mask_pos, user_items_mask_neg, time_cost = self.getTrainData(batchId)
        self.optimizer.run(feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
            self.j_id: batch_j,
            self.u_items_ids: batch_u_items,
            self.review_input: batch_u_reviews,
            self.dropout_keep_prob: self.dropout_keep_prob_input,
            self.u_pos_items_ids_mask: user_items_mask_pos,
            self.u_neg_items_ids_mask: user_items_mask_neg,
            self.batch_num: self.trainBatchSize
        })
        ratingLoss = self.rating_loss.eval(feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
            self.j_id: batch_j,
            self.u_items_ids: batch_u_items,
            self.review_input: batch_u_reviews,
            self.dropout_keep_prob: self.dropout_keep_prob_input,
            self.u_pos_items_ids_mask: user_items_mask_pos,
            self.u_neg_items_ids_mask: user_items_mask_neg,
            self.batch_num: self.trainBatchSize
        })
        factor_l2_loss = self.factor_l2_loss.eval(feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
            self.j_id: batch_j,
            self.u_items_ids: batch_u_items,
            self.review_input: batch_u_reviews,
            self.dropout_keep_prob: self.dropout_keep_prob_input,
            self.u_pos_items_ids_mask: user_items_mask_pos,
            self.u_neg_items_ids_mask: user_items_mask_neg,
            self.batch_num: self.trainBatchSize
        })
        review_l2_loss = self.review_l2_loss.eval(feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
            self.j_id: batch_j,
            self.u_items_ids: batch_u_items,
            self.review_input: batch_u_reviews,
            self.dropout_keep_prob: self.dropout_keep_prob_input,
            self.u_pos_items_ids_mask: user_items_mask_pos,
            self.u_neg_items_ids_mask: user_items_mask_neg,
            self.batch_num: self.trainBatchSize
        })
        loss = ratingLoss + factor_l2_loss + review_l2_loss

        if batchId == 0 and print_flag == 1:
        # if True:
            self.u_id_print.eval(feed_dict={
                self.u_id: batch_u,
            })
            self.positive_i_print.eval(feed_dict={
                self.i_id: batch_v,
            })
            self.item_ids_print.eval(feed_dict={
                self.u_items_ids: batch_u_items,
            })
            self.u_pos_items_ids_mask_print.eval(feed_dict={
                self.u_pos_items_ids_mask: user_items_mask_pos
            })
            self.u_neg_items_ids_mask_print.eval(feed_dict={
                self.u_neg_items_ids_mask: user_items_mask_neg
            })
            # print detail info of review
            if self.verbose:
                for i in range(self.item_pad_num):
                    self.review_wordId_print[i].eval(feed_dict={
                        self.u_id: batch_u,
                        self.i_id: batch_v,
                        self.j_id: batch_j,
                        self.u_items_ids: batch_u_items,
                        self.review_input: batch_u_reviews,
                        self.dropout_keep_prob: self.dropout_keep_prob_input,
                        self.u_pos_items_ids_mask: user_items_mask_pos,
                        self.u_neg_items_ids_mask: user_items_mask_neg,
                        self.batch_num: self.trainBatchSize
                    })
                for i in range(self.item_pad_num):
                    self.review_input_print[i].eval(feed_dict={
                        self.u_id: batch_u,
                        self.i_id: batch_v,
                        self.j_id: batch_j,
                        self.u_items_ids: batch_u_items,
                        self.review_input: batch_u_reviews,
                        self.dropout_keep_prob: self.dropout_keep_prob_input,
                        self.u_pos_items_ids_mask: user_items_mask_pos,
                        self.u_neg_items_ids_mask: user_items_mask_neg,
                        self.batch_num: self.trainBatchSize
                    })
                # for i in range(self.item_pad_num):
                #     self.component_raw_output[i].eval(feed_dict={
                #         self.u_id: batch_u,
                #         self.i_id: batch_v,
                #         self.j_id: batch_j,
                #         self.u_items_ids: batch_u_items,
                #         self.review_input: batch_u_reviews,
                #         self.dropout_keep_prob: self.dropout_keep_prob_input,
                #         self.u_items_ids_mask: batch_u_items_bask,
                #         self.batch_num: self.trainBatchSize
                #     })
                for i in range(self.item_pad_num):
                    self.review_output_print[i].eval(feed_dict={
                        self.u_id: batch_u,
                        self.i_id: batch_v,
                        self.j_id: batch_j,
                        self.u_items_ids: batch_u_items,
                        self.review_input: batch_u_reviews,
                        self.dropout_keep_prob: self.dropout_keep_prob_input,
                        self.u_pos_items_ids_mask: user_items_mask_pos,
                        self.u_neg_items_ids_mask: user_items_mask_neg,
                        self.batch_num: self.trainBatchSize
                    })
                for i in range(self.item_pad_num):
                    self.review_atten_print[i].eval(feed_dict={
                        self.u_id: batch_u,
                        self.i_id: batch_v,
                        self.j_id: batch_j,
                        self.u_items_ids: batch_u_items,
                        self.review_input: batch_u_reviews,
                        self.dropout_keep_prob: self.dropout_keep_prob_input,
                        self.u_pos_items_ids_mask: user_items_mask_pos,
                        self.u_neg_items_ids_mask: user_items_mask_neg,
                        self.batch_num: self.trainBatchSize
                    })
            self.atten_i_print.eval(feed_dict={
                self.u_id: batch_u,
                self.i_id: batch_v,
                self.j_id: batch_j,
                self.u_items_ids: batch_u_items,
                self.review_input: batch_u_reviews,
                self.dropout_keep_prob: self.dropout_keep_prob_input,
                self.u_pos_items_ids_mask: user_items_mask_pos,
                self.u_neg_items_ids_mask: user_items_mask_neg,
                self.batch_num: self.trainBatchSize
            })

            self.atten_j_print.eval(feed_dict={
                self.u_id: batch_u,
                self.i_id: batch_v,
                self.j_id: batch_j,
                self.u_items_ids: batch_u_items,
                self.review_input: batch_u_reviews,
                self.dropout_keep_prob: self.dropout_keep_prob_input,
                self.u_pos_items_ids_mask: user_items_mask_pos,
                self.u_neg_items_ids_mask: user_items_mask_neg,
                self.batch_num: self.trainBatchSize
            })
            print_flag = 0
        end = time.time()

        if epochId % 5 == 0 and batchId == 0:
            self.logger.info("----------------------------------------------------------------------")
            self.logger.info("time of collect a batch of data: " + time_cost + " seconds")
            self.logger.info(
                "rating_loss: %.4f   factor_l2_loss: %.4f   cnn_l2_loss: %.4f " % (
                    ratingLoss, factor_l2_loss, review_l2_loss))
            self.logger.info(
                "batchId: %d epoch %d/%d   batch_loss: %.4f   time of a batch: %.4f" % (
                    batchId, epochId, self.maxIter, loss, (end - start)))
            self.evaluateRanking(epochId, batchId)
        return loss

    def feature_atten_output(self, user_s, user_n, item_s, item_n, pred_factor):
        feature_vec = tf.concat(values=[user_s, user_n, item_s, item_n], axis=1)
        with tf.variable_scope("feature_attention_weights", reuse=True):
            W = tf.get_variable(name="feature_atten_W", shape=[self.numFactor * 4, self.numFactor], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())
            b = tf.get_variable(name="feature_atten_b", shape=[self.numFactor], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())

            atten_vec = tf.nn.softmax(tf.nn.relu(tf.matmul(feature_vec, W) + b))

            atten_pred_factor = tf.multiply(pred_factor, atten_vec)

            return atten_pred_factor



    def attentionOutputs(self, user_embedding, curr_item_embedding, curr_item_review, reviewEmbeddings, flag):
        attention_output_list = []
        with tf.variable_scope("item_attention_weights", reuse=True):
            user_W = tf.get_variable(name="user_W", shape=[self.numFactor, self.numFactor // 2], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())
            item_W = tf.get_variable(name="item_W", shape=[self.numFactor, self.numFactor // 2], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())
            embed_bias = tf.get_variable(name="embed_bias", dtype=tf.float32,
                                         initializer=tf.constant(0.0, shape=[self.numFactor // 2]))
            summary_W = tf.get_variable(name="summary_W", shape=[self.numFactor // 2, 1], dtype=tf.float32,
                                        initializer=tf.contrib.layers.xavier_initializer())
            summary_b = tf.get_variable(name="summary_b", dtype=tf.float32,
                                        initializer=tf.constant(0.0))

            user_atten = tf.matmul(user_embedding, user_W)
            curr_item_atten = tf.matmul(curr_item_embedding, item_W)
            curr_item_review_atten = tf.matmul(curr_item_review, item_W)
            attention_outputs = None

            split_list = [1] * self.item_pad_num
            itemIds = tf.split(self.u_items_ids, split_list, 1)
            for i in range(self.item_pad_num):
                hist_item_embed = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, itemIds[i]),
                                             [-1, self.numFactor])
                hist_item_atten = tf.matmul(hist_item_embed, item_W)
                review_atten = tf.matmul(reviewEmbeddings[i], item_W)
                self.review_atten_print[i] = tf.Print(review_atten, [review_atten],
                                                      message="review_atten_print%d" % (i),
                                                      summarize=self.numFactor // 2)
                atten_embed = tf.nn.relu(
                    user_atten + curr_item_atten + curr_item_review_atten + hist_item_atten + review_atten + embed_bias)

                atten_output = tf.matmul(atten_embed, summary_W) + summary_b

                if attention_outputs == None:
                    attention_outputs = atten_output
                else:
                    attention_outputs = tf.concat([attention_outputs, atten_output], 1)

            if flag == "p":
                attentionValues = self.softmask(mask=self.u_pos_items_ids_mask, attentions=attention_outputs)
                self.atten_i_print = tf.Print(attentionValues, [attentionValues], message="attention_i",
                                              summarize=self.item_pad_num)
            else:
                attentionValues = self.softmask(mask=self.u_neg_items_ids_mask, attentions=attention_outputs)
                self.atten_j_print = tf.Print(attentionValues, [attentionValues], message="attention_j",
                                              summarize=self.item_pad_num)

            attention_output_list.extend(tf.split(attentionValues, split_list, 1))

            return attention_output_list

    def new_attention_output(self, user_embedding, curr_item_embedding, reviewEmbeddings, flag):
        attention_output_list = []
        with tf.variable_scope("item_attention_weights", reuse=True):
            user_W = tf.get_variable(name="user_W", shape=[self.numFactor, self.numFactor // 2], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())
            item_W = tf.get_variable(name="item_W", shape=[self.numFactor, self.numFactor // 2], dtype=tf.float32,
                                     initializer=tf.contrib.layers.xavier_initializer())
            embed_bias = tf.get_variable(name="embed_bias", dtype=tf.float32,
                                        initializer=tf.constant(0.0, shape=[self.numFactor // 2]))
            summary_W = tf.get_variable(name="summary_W", shape=[self.numFactor // 2, 1], dtype=tf.float32,
                                        initializer=tf.contrib.layers.xavier_initializer())
            summary_b = tf.get_variable(name="summary_b", dtype=tf.float32,
                                        initializer=tf.constant(0.0))

            user_atten = tf.matmul(user_embedding, user_W)
            curr_item_atten = tf.matmul(curr_item_embedding, item_W)
            #curr_item_review_atten = tf.matmul(curr_item_review, item_W)
            attention_outputs = None

            split_list = [1] * self.item_pad_num
            itemIds = tf.split(self.u_items_ids, split_list, 1)
            for i in range(self.item_pad_num):
                hist_item_embed = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, itemIds[i]), [-1, self.numFactor])
                hist_item_atten = tf.matmul(hist_item_embed, item_W)
                review_atten = tf.matmul(reviewEmbeddings[i], item_W)
                self.review_atten_print[i] = tf.Print(review_atten, [review_atten], message="review_atten_print%d"%(i), summarize=self.numFactor // 2)
                #atten_embed = tf.nn.relu(user_atten + curr_item_atten + curr_item_review_atten + hist_item_atten + review_atten + embed_bias)
                atten_embed = tf.nn.relu(user_atten + curr_item_atten + hist_item_atten + review_atten + embed_bias)

                atten_output = tf.matmul(atten_embed, summary_W) + summary_b

                if attention_outputs == None:
                    attention_outputs = atten_output
                else:
                    attention_outputs = tf.concat([attention_outputs, atten_output], 1)

            if flag == "p":
                attentionValues = self.softmask(mask=self.u_pos_items_ids_mask, attentions=attention_outputs)
                self.atten_i_print = tf.Print(attentionValues, [attentionValues], message="attention_i", summarize=self.item_pad_num)
            else:
                attentionValues = self.softmask(mask=self.u_neg_items_ids_mask, attentions=attention_outputs)
                self.atten_j_print = tf.Print(attentionValues, [attentionValues], message="attention_j", summarize=self.item_pad_num)

            attention_output_list.extend(tf.split(attentionValues, split_list, 1))

            return attention_output_list

    def no_attentionOutputs(self, flag):
        itemIds = []
        item_embeddings = []
        attentionValues = None
        attentionOutputs = []

        split_list = [1] * self.item_pad_num

        itemIds.extend(tf.split(self.u_items_ids, split_list, 1))

        for itemId in itemIds:
            itemEmbedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, itemId), [-1, self.numFactor])
            item_embeddings.append(itemEmbedding)
            attentionValue = tf.ones(shape=[self.batch_num, 1])
            if attentionValues is None:
                attentionValues = attentionValue
            else:
                attentionValues = tf.concat([attentionValues, attentionValue], 1)
        attentionValues = self.softmask(mask=self.u_pos_items_ids_mask, attentions=attentionValues)
        if flag == "p":
            attentionValues = self.softmask(mask=self.u_pos_items_ids_mask, attentions=attentionValues)
            self.atten_i_print = tf.Print(attentionValues, [attentionValues], message="attention_i", summarize=self.item_pad_num)
        else:
            attentionValues = self.softmask(mask=self.u_pos_items_ids_mask, attentions=attentionValues)
            self.atten_j_print = tf.Print(attentionValues, [attentionValues], message="attention_j", summarize=self.item_pad_num)
        attentionOutputs.extend(tf.split(attentionValues, split_list, 1))

        return attentionOutputs, item_embeddings

    def getPredList_ByUserIdxList(self, user_idices):
        # build test batch
        end0 = time.time()
        testBatch_user = []
        batch_u_reviews_total = []
        batch_u_items_total = []
        batch_u_items_total_mask = []
        batch_num = len(user_idices) * 100

        for i in range(len(user_idices)):
            userIdx = user_idices[i]
            for itemIdx in self.evalItemsForEachUser[userIdx]:
                testBatch_user.append([userIdx, itemIdx, 1.0])
            batch_u_reviews = self.user_review_vecs_eval[userIdx]
            batch_u_items = self.user_item_vecs_eval[userIdx]
            batch_u_reviews_total.extend(batch_u_reviews)
            batch_u_items_total.extend(batch_u_items)
            batch_u_items_total_mask.extend([self.user_review_mask[userIdx]] * 100)

        batch_u = np.array(testBatch_user)[:, 0:1].astype(np.int32)
        batch_v = np.array(testBatch_user)[:, 1:2].astype(np.int32)
        batch_u_reviews_total = np.array(batch_u_reviews_total)
        batch_u_items_total = np.array(batch_u_items_total)
        batch_u_items_total_mask = np.array(batch_u_items_total_mask)
        end1 = time.time()

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
            self.u_items_ids: batch_u_items_total,
            self.review_input: batch_u_reviews_total,
            self.u_pos_items_ids_mask: batch_u_items_total_mask,
            self.batch_num: batch_num
        })

        end2 = time.time()

        output_lists = []
        for i in range(len(user_idices)):
            recommendList = {}
            start = i * 100
            end = start + 100
            for j in range(start, end):
                recommendList[batch_v[j][0]] = predList[j][0]
            sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
            output_lists.append(sorted_RecItemList)

        end3 = time.time()

        return output_lists, end1 - end0, end2 - end1, end3 - end2

    def getPredList_ByUserIdx(self, userIdx):

        # build test batch
        testBatch_user = []

        for itemIdx in self.evalItemsForEachUser[userIdx]:
            testBatch_user.append([userIdx, itemIdx, 1.0])

        batch_u = np.array(testBatch_user)[:, 0:1]
        batch_v = np.array(testBatch_user)[:, 1:2]
        # with review input

        batch_u_reviews = [self.user_items_reviewVectors[userIdx] for i in range(len(testBatch_user))]
        batch_u_items = [self.user_items_train[userIdx] for i in range(len(testBatch_user))]

        batch_u_reviews = np.array(batch_u_reviews)
        batch_u_items = np.array(batch_u_items)

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
            self.u_items_ids: batch_u_items,
            self.review_input: batch_u_reviews,
        })

        recommendList = {}
        for i in range(len(testBatch_user)):
            recommendList[batch_v[i][0]] = predList[i][0]

        sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
        # print("user " + str(userIdx) + "/" + str(self.numUser-1) + " finished")

        return sorted_RecItemList

    def get_user_vectors_for_eval(self):
        user_reviewVecs = {}
        user_itemVecs = {}
        for userIdx in self.user_items_test:
            user_reviewVecs[userIdx] = [self.user_items_reviewVectors[userIdx]] * 100
            user_itemVecs[userIdx] = [self.user_items_train_paded[userIdx]] * 100
        return user_reviewVecs, user_itemVecs

    def softmask(self, mask, attentions):

        attention_mask = tf.where(mask, attentions, tf.ones(shape=[self.batch_num, self.item_pad_num]) * np.NINF)
        attention_mask = tf.nn.softmax(attention_mask)

        return attention_mask

    def cosinSim(self, tensor1, tensor2):
        nor_1 = tf.nn.l2_normalize(tensor1, dim=1)
        nor_2 = tf.nn.l2_normalize(tensor2, dim=1)

        return tf.reduce_sum(tf.multiply(nor_1, nor_2), axis=1, keep_dims=True)
