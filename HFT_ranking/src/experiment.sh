#!/bin/bash

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PWD/liblbfgs-1.10/lib/.libs/

for dataDir in ./hft_9_12/userRatio_office_products_raw ./hft_9_12/userRatio_android_apps_raw ./hft_9_12/userRatio_beauty_raw ./hft_9_12/userRatio_tools_home_improvement_raw
do
	for latentReg in 0.01 0.1 1
	do
		for lambda in 0.01 0.1 1
		do
			for K in 10
			do
				./train ${dataDir}/full.txt $latentReg $lambda $K ./ ./ ${dataDir}/train.txt ${dataDir}/valid.txt ${dataDir}/test.txt >> log.txt
			done
		done
	done
done




