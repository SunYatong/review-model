#!/bin/bash
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PWD/liblbfgs-1.10/lib/.libs/
#9
fileName=movies
dataDir=./dataset/${fileName}
latentReg=0.001
lambda=10
K=32
./train ${dataDir}/hft_full.txt ${dataDir}/hft_train.txt ${dataDir}/hft_test.txt ${dataDir}/hft_test.txt $latentReg $lambda $K ./model.out ./predictions.out ${dataDir}/eval.txt
