Bicycles last forever

With the rapid development of science and techniques, human transportation is becoming faster and more convenient.
Therefore, some people believe that bicycles will be gradually abandoned from human life.
However, they ignore the fact that bikes have some specific superiority in comparison with high-tech vehicles.
Additionally, they also have many other significant functions beyond transportation in our daily life.

First of all, bicycles are more adaptive in some scenarios.
In some early-developed cities, there are many narrow streets which lead to busy and crowded traffic.
Vehicles like private cars, buses and trucks are too big to pass and too fast to control in such restricted areas, while bicycles can pass through them safe and sound.
bicycles can also reduce the traffic pressure due to their small size.

Secondly, bicycles are environmentally friendly.
In some developing countries like China, the number of cars is increasing so fast which leads to huge consumption of fossil fuels and carbon emissions.
To solve these environmental problems, on the one hand, Chinese government encourages people to travel with bicycles and support some companies to develop sharing bikes.
On the other hand, it limits the car number by issuing an odd-and-even license plate rule.

Thirdly, bicycles are beneficial for human health, both physically and mentally.
Nowadays, a bicycle is not so much a transport tool as a bodybuilding equipment.
More and more people are pursuing a healthy lifestyle and tend to go out with their bikes, instead of cars deliberately.
Moreover, it is an excellent way for families and friends to cultivate deeper feelings by traveling with bikes together.

In conclusion, we can benefit from bicycles in terms of many perspectives and their unique advantages can hardly be substituted by other tools.
Therefore, bicycles will not only continue to exist but also become more necessary in our lives.
