import math
from scipy import stats

def get_t(a, b):
    return (a-b) / (math.sqrt(0.005 * 0.005 * 0.1 * 2))

if __name__ == '__main__':
    t = get_t(0.2837, 0.3956)
    print(t)

    p = stats.t.cdf(t, 9)
    print(p)