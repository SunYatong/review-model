from recommender.ReviewBPR_NEW import ReviewBPR_NEW
from recommender.DeepCoNN import DeepCoNN
from recommender.BPR import BPRRecommender
from recommender.SVDPP import SVDPP
from data_model.ReviewDataModel import ReviewDataModel

def runParm(config, fileName, lam):
    config['fileName'] = fileName
    dataModel = ReviewDataModel(config)
    dataModel.buildModel()
    config['factor_lambda'] = lam
    config['atten_lambda'] = lam
    config['review_lambda'] = lam
    config['dropout_keep_prob'] = 0.8
    rec = ReviewBPR_NEW(dataModel, config)
    rec.run()

if __name__ == '__main__':

    # digital_music_5_core_100, baby_5_core_100 apps_for_android_5
    # kindle_5 movies_tv_5
    config = {
        'negative_sample': 0,
        'splitterType': 'userTimeRatio',
        'fileName': 'electronics_5',
        'trainType': 'test',
        'threshold': 0,
        'wordVecSize': 200,
        'learnRate': 0.001,
        'maxIter': 500,
        'trainBatchSize': 10,
        'testBatchSize': 10,
        'numFactor': 32,
        'topN': 10,
        'factor_lambda': 0.1,
        'atten_lambda': 0.1,
        'goal': 'ranking',
        'addBias': False,
        'with_review_input': True,
        'attention_version': 'new',
        'dropout_keep_prob': 0.8,
        'verbose': False,
        'item_pad_num': 14,
        'if_full_review': True,
        'review_lambda': 0.1,

        'component_type': 'RNN',

        'rnn_unit_num': 48,
        'rnn_layer_num': 1,
        'rnn_cell': 'GRU',

        'filters_sizes': [2, 3],
        'num_filters': 50,
        'atten_level': 'no_current',
        'add_relu': False,
        'feature_atten': False,

        'random_seed': 1,
        'eval_item_num': 100,
        'early_stop': True,

        'save_model': False,
        'load_model': False,
    }


    for fileName in ["kindle_5", "electronics_5", "apps_for_android_5", "cd_vinyl_5", "movies_tv_5"]:
        config['fileName'] = fileName
        dataModel = ReviewDataModel(config)
        dataModel.buildModel()

        for seed in range(10):
            config['random_seed'] = seed
            rec = SVDPP(dataModel, config)
            rec.run()






