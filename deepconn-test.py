from recommender.DeepCoNN import DeepCoNN
import data_model.ReviewDataModel

if __name__ == '__main__':

    # digital_music_5_core_100, baby_5_core_100
    config = {
        'negative_sample': 0,
        'splitterType': 'userTimeRatio',
        'fileName': 'apps_for_android_5',
        'trainType': 'test',
        'threshold': 0,
        'wordVecSize': 200,
        'learnRate': 0.001,
        'maxIter': 500,
        'trainBatchSize': 1000,
        'testBatchSize': 100,
        'numFactor': 32,
        'filters_sizes': [1, 2],
        'num_filters': 5,
        'topN': 10,
        'factor_lambda': 0.1,
        'review_lambda': 0.1,
        'atten_lambda': 0.1,
        'dropout_keep_prob': 0.8,
        'goal': 'ranking',
        'addBias': False,
        'with_review_input': True,
        'attention_version': 'new',
        'lam1': 0.0,
        'verbose': False,
        'item_pad_num': 8,
        'if_full_review': True,
        'fm_k': 16,

        'random_seed': 1,
        'eval_item_num': 100,
        'early_stop': True,

        'save_model': False,
        'load_model': False,
    }

    for fileName in ["kindle_5", "electronics_5", "apps_for_android_5", "cd_vinyl_5", "movies_tv_5"]:
        config['fileName'] = fileName
        dataModel = data_model.ReviewDataModel.ReviewDataModel(config)
        dataModel.buildModel()

        for dropout in [0.8]:
            for fm_k in [16]:
                for seed in range(10):
                    config['random_seed'] = seed
                    config['dropout_keep_prob'] = dropout
                    config['fm_k'] = fm_k
                    rec = DeepCoNN(dataModel, config)
                    rec.run()


        # rec = DeepCoNN(dataModel, config)
        # rec.run()

    # for learnrate in [0.0001, 0.001, 0.01, 0.1]:
    #     for batchSize in [2000, 1000, 500, 250, 128]:
    #         config['learnRate'] = learnrate
    #         config['trainBatchSize'] = batchSize
    #         rec = recommender.ReviewBPR_CNN.ReviewBPR_CNN(dataModel, config)
    #         rec.run()










# 1.0, 0.9, 0.8, 0.7, 0.6, 0.5







